﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Presentation;
using ggReceptionHelper;

namespace ggReceptionAssistant {
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : ModernWindow {
    //public HLP_SQLite sqlHelper;
    //public HLP_Escaux telHelper;
    public HelperClass_Reception helper;
    public bool isAdminVersion;
    
    DispatcherTimer tmrMaintenance;
    DateTime AEOS_update;
    DateTime SOP_update;

    public MainWindow() {
      InitializeComponent();

      //sqlHelper = new HLP_SQLite("Data Source=ReceptionAssistant.db3;Version=3;DateTimeFormat=Ticks;UseUTF16Encoding=False;", true);
      //telHelper = new HLP_Escaux();
      helper = new HelperClass_Reception();
      helper.SQLite_Database = "Data Source=ReceptionAssistant.db3;Version=3;DateTimeFormat=Ticks;UseUTF16Encoding=False;";
      helper.initSQLite(true);
      helper.AEOS_Database = helper.getSetting("AEOS_CONNECT");
      
      string[] args = Environment.GetCommandLineArgs();

      isAdminVersion = false;
      foreach (string arg in args) {
        if (arg.Contains("admin")) {
          isAdminVersion = true;
        }
      }

      if (helper.AEOS_Database == "no_value") {
        this.ContentSource = new Uri("/Settings/ucSettings.xaml", UriKind.Relative);
        return;
      }

#if DEBUG
      Link testing = new Link();
      testing.DisplayName = "debug";
      testing.Source = new Uri("/General/ucCallTesting.xaml", UriKind.Relative);
      this.MenuLinkGroups[0].Links.Add(testing);
#endif

      string mySOP = helper.getSetting("RAPHONESOP"); 
      Application.Current.Resources["sop_comp"] = mySOP;
      Application.Current.Resources["sop_ip"] = helper.getSOPip(mySOP, true);
      Application.Current.Resources["phone_id"] = helper.getSetting("RAPHONEID");

      try {
        if (pingSOP(Application.Current.Resources["sop_ip"].ToString()) != "") {
          Application.Current.Resources["sop_ip"] = "0.0.0.0";
          this.ContentSource = new Uri("/Settings/ucSettings.xaml", UriKind.Relative);
          return;
        }
          
      } 
      catch (Exception ex) {
        string error = ex.Message;
        Application.Current.Resources["sop_ip"] = "0.0.0.0";
        return;

      }

      AEOS_update = DateTime.Now;
      SOP_update = DateTime.Now.AddMinutes(5);

      tmrMaintenance = new DispatcherTimer();
      tmrMaintenance.Interval = new TimeSpan(0, 0, 10);
     // tmrMaintenance.Interval = new TimeSpan(0, 0, 0, 0, 100);
      tmrMaintenance.Tick += TmrMaintenance_Tick;

      tmrMaintenance.IsEnabled = true;

    }

    private void TmrMaintenance_Tick(object sender, EventArgs e) {
      tmrMaintenance.IsEnabled = false;

      DataTable sopList = getPrimarySops();
      if (sopList.Rows.Count == 0) {
        if (this.ContentSource != new Uri("/Settings/ucSettings.xaml", UriKind.Relative)) {
          this.ContentSource = new Uri("/Settings/ucSettings.xaml", UriKind.Relative);
        }
      }
      foreach (DataRow primSop in sopList.Rows) {
        BackgroundWorker pingWorker = new BackgroundWorker();
        pingWorker.WorkerReportsProgress = false;
        pingWorker.DoWork += PingWorker_DoWork;
        pingWorker.RunWorkerCompleted += PingWorker_RunWorkerCompleted;
        pingWorker.RunWorkerAsync(primSop["ip"].ToString());
      }

      if (DateTime.Now > AEOS_update) {
        BackgroundWorker secuSyncer = new BackgroundWorker();
        secuSyncer.WorkerReportsProgress = false;
        secuSyncer.DoWork += SecuSyncer_DoWork;
        //secuSyncer.RunWorkerCompleted += SecuSyncer_RunWorkerCompleted;
        secuSyncer.RunWorkerAsync();
               
        AEOS_update = AEOS_update.AddMinutes(10);
      }

      if (DateTime.Now > SOP_update) {
        BackgroundWorker sopSyncer = new BackgroundWorker();
        sopSyncer.WorkerReportsProgress = false;
        sopSyncer.DoWork += SopSyncer_DoWork;
        //sopSyncer.RunWorkerCompleted += SopSyncer_RunWorkerCompleted;
        sopSyncer.RunWorkerAsync();

        SOP_update = SOP_update.AddMinutes(10);
      }

      tmrMaintenance.IsEnabled = true;
    }

    #region background workers
    private void SopSyncer_DoWork(object sender, DoWorkEventArgs e) {
      DataTable SOPs = helper.getSOPlist();
      DataTable sopUsers = new DataTable();
      DataTable secuUsers = helper.getUserList();
      try {
        foreach (DataRow subSOP in SOPs.Select("isprimary=1")) {
          DataTable tmpUsers = helper.getUserDirectory(subSOP["ip"].ToString());

          foreach (DataRow sopUser in tmpUsers.Rows) {
            sopUser["sop"] = subSOP["company"].ToString();
          }
          tmpUsers.AcceptChanges();

          try {
            sopUsers.Merge(tmpUsers);
          }
          catch (Exception ex) {
            string error = ex.Message;
          }

          tmpUsers.Dispose();
        }
        helper.syncPhoneUsers(ref sopUsers, ref secuUsers);
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
      finally {
        SOPs.Dispose();
        sopUsers.Dispose();
        secuUsers.Dispose();
      }
    }

    private void SecuSyncer_DoWork(object sender, DoWorkEventArgs e) {
      try {
        helper.openSecuConnection();
        helper.syncSecuUsers();
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
      finally {
        helper.closeSecuConnection();
      }
    }

    private void PingWorker_DoWork(object sender, DoWorkEventArgs e) {
      e.Result = pingSOP(e.Argument.ToString());
    }

    private void PingWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
      if (e.Result.ToString() != "") {
        Application.Current.Resources["sop_ip"] = "0.0.0.0";
        tmrMaintenance.IsEnabled = false;
        ModernDialog.ShowMessage("ping to " + e.Result.ToString() + " failed", "error", MessageBoxButton.OK);
        this.ContentSource = new Uri("/Settings/ucSettings.xaml", UriKind.Relative);
        //Application.Current.Resources["sop_ip"] = "0.0.0.0";
        //tmrMaintenance.IsEnabled = false;
      } else {
      //  Application.Current.Resources["sop_ip"] = e.Result.ToString();
      }

    }
    #endregion

    #region tasks routines
    DataTable getPrimarySops() {
      DataTable result;
      try {
        result = helper.getSOPlist();
      }
      catch {
        result = new DataTable();
      }

      result.DefaultView.RowFilter = "isprimary = 1";
      return result.DefaultView.ToTable();
    }

    private string pingSOP(string sopIP) {
      string result = "";

      Ping primaryPing = new Ping();

      string data = "intrimmo--GG126--W89--W91--intrion";
      byte[] buffer = Encoding.ASCII.GetBytes(data);
      PingReply reply = primaryPing.Send(sopIP, 5000, buffer);

      if (reply.Status != IPStatus.Success) {
        result = sopIP;
      }

      return result;
    }

    #endregion

  }
}
