﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Xml;
using System.Xml.XPath;

/*
  get history call
  http://172.30.30.3/xml/dbGetCallHistory.php?start_date=2015-12-22 15:30:00&ext=004&limit=100
*/

namespace ggReceptionHelper {

  public partial class HelperClass_Reception : IDisposable {

    public struct userConfig {
      public string callflowUser;
      public string callflowType;
      public string callflowColor;
    }

    public struct phoneData {
      public string[] callerID;
      public string[] lastCall;
      public string[] localChannel;
      public string[] remoteChannel;
      public string[] state;
    }

    public struct parkData {
      public string channelID;
      public string callerID;
      public int parkSlot;
    }

    userConfig userCallflow;
    string userStatus;                     /// status van de user
  
    /*
    public SopDirectoryInfo getUserInfo(string email) {
      string error = "";
      string request = createRequest("getDirectoryInfo", email);
      XmlDocument requestResult = doRequest(request, out error);

      if (error == "")
        return processDirectoryInfo(requestResult);
      else
        return null;

    }*/

    /*
    public void changeUserStatus(ref SopDirectoryInfo userInfo, string newStatus) {
      string requestError = "";
      string request;
      XmlDocument response;

      // 1) wijzig de status
      request = createRequest("setProfile", userInfo.ext) + newStatus;
      response = doRequest(request, out requestError);
      if (requestError != "") {
        userInfo.error = requestError;
        return;
      }
      requestError = processProfileResult(response);

      // 2) extra check callflow
      request = createRequest("getCallflow", userInfo.ext);
      response = doRequest(request, out requestError);
      if (requestError != "") {
        userInfo.error = requestError;
        return;
      }
      requestError = processCallflowResult(response);
      if (requestError != "") {
        userInfo.error = requestError;
        return;
      }

      // 3) get profile parameters
      request = createRequest("getProfileParams", userInfo.ext);
      response = doRequest(request, out requestError);
      if (requestError != "") {
        userInfo.error = requestError;
        return;
      }
      processProfileParameters(response);

      // 4a) indien status = HOLIDAY
      request = "";
      if (userStatus.ToUpper() == "HOLIDAY") {
        if (userCallflow.callflowType == "User") {

          // nakijken of de huidige actie overeenkomt met de 'te wijzigen' actie
          if (userSecondAction == userNormal) {
            request = createRequest("setSecondAction", userInfo.ext) + userHoliday;
          }
        } else {
          if (userSecondAction == mgmtNormal) {
            request = createRequest("setSecondAction", userInfo.ext) + mgmtHoliday;
          }
        }

      } else {
        // 4b) anders...
        if (userCallflow.callflowType == "User") {
          if (userSecondAction == userHoliday) {
            request = createRequest("setSecondAction", userInfo.ext) + userNormal;
          }
        } else {
          if (userSecondAction == userHoliday) {
            request = createRequest("setSecondAction", userInfo.ext) + mgmtNormal;
          }
        }
      }

      // 4c) ... enkel als er iets uit te voeren valt...
      if (request != "") {
        response = doRequest(request, out requestError);
        if (requestError != "") {
          userInfo.error = requestError;
          return;
        }
      }

      // 5) telefoonnummer receptie nakijken
      if (userReception != receptionPhone) {
        request = createRequest("setReception", userInfo.ext) + receptionPhone;

        response = doRequest(request, out requestError);
        if (requestError != "") {
          userInfo.error = requestError;
          return;
        }
      }
    }
    */

    #region GENERAL actions
    private string createRequest(string sopIP, string kind, string info = "") {
      string result = "http://" + sopIP;

      switch (kind) {
      case "getDirectoryInfo":
        result += "/xml/dbGetDirectoryInfo.php";
        break;

      //case "setProfile":
      //  result += "/xml/dbSetProfile.php?ext=" + info.PadLeft(3, '0') + "&status=";
      //  break;

      case "getPhoneState":
        result += "/xml/dbGetPhoneState.php?phone_id=" + info;
        break;

      case "getCallflow":
        result += "/xml/dbGetCallflow.php?ext=" + info.PadLeft(3, '0');
        break;

      case "callInitiate":
        result += "/xml/ccOriginate.php?phone_id=" + info;
        break;

      case "callRedirect":
        // http://sop.ip.address/xml/ccRedirect.php?channel_id=[channel_id]&to=[number]
        result += "/xml/ccRedirect.php?channel_id=" + info;
        break;

      case "callPark":
        // http://sop.ip.address/xml/ccPark.php?channel_id=[channel_id]
        result += "/xml/ccPark.php?channel_id=" + info;
        break;

      case "getParkedChannels":
        // http://sop.ip.address/xml/dbGetParkedChannels.php
        result += "/xml/dbGetParkedChannels.php";
        break;

      case "callJoin":
        // http://sop.ip.address/xml/ccJoinPark.php?channel_id=[channel_id]&to=[ParkSlot]
        result += "/xml/ccJoinPark.php?channel_id=" + info;
        break;

      case "showCamera":
        result = "https://" + sopIP + info;
        break;
        //case "getProfileParams":
        //  result += "/xml/dbGetProfileParameters.php?ext=" + info.PadLeft(3, '0') + "&show_admin=1";
        //  break;

        //case "setSecondAction":
        //    result += "/xml/dbSetProfileParameters.php?ext=" + info.PadLeft(3, '0') +
        //      "&Unify" + userCallflow.callflowType + userCallflow.callflowColor + "Secondaction=";
        //  break;

        //case "setReception":
        //  result += "/xml/dbSetProfileParameters.php?ext=" + info.PadLeft(3, '0') +
        //    "&Unify" + userCallflow.callflowType + userCallflow.callflowColor + "ReceptionExtension=";
        //  break;
      }

      return result;
    }

    private XmlDocument doRequest(string request, out string error) {
      XmlDocument result = null;

      error = "";
      try {
        HttpWebRequest sopRequest = WebRequest.Create(request) as HttpWebRequest;
        HttpWebResponse sopResponse = sopRequest.GetResponse() as HttpWebResponse;

        result = new XmlDocument();
        result.Load(sopResponse.GetResponseStream());
      }
      catch (Exception ex) {
        error = ex.Message + " (" + request + ")";
      }

      return result;
    }

    private bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) {
      return true;
    }

    private MemoryStream doCamRequest(string camIP, string APIcommand) {
      MemoryStream result = null;

      string request = "https://" + camIP + APIcommand;
      try {
        ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);

        var camRequest = WebRequest.Create(request);
        camRequest.Credentials = new NetworkCredential("intrimmo", "GG126");
        camRequest.Method = "GET";
        var sopResponse = camRequest.GetResponse();

        if (sopResponse.ContentType == "application/json") {
          var jsonResult = sopResponse.GetResponseStream();
        } else {
          result = new MemoryStream();
          sopResponse.GetResponseStream().CopyTo(result);
        }

      }
      catch (Exception ex) {
        string error = ex.Message + " (" + request + ")";
      }

      return result;
    }
    #endregion

    #region USER DIRECTORY actions
    public DataTable getUserDirectory(string sopIP) {
      DataTable result = new DataTable("directory");
      string error = "";
      string request = createRequest(sopIP, "getDirectoryInfo", "");
      XmlDocument requestResult = doRequest(request, out error);

      if (error == "")
        result = processDirectoryInfo(requestResult);

      return result;
    }

    private DataTable processDirectoryInfo(XmlDocument response) {
      DataTable result = new DataTable("directory");

      result.Columns.Add("sop");
      result.Columns.Add("firstname");
      result.Columns.Add("lastname");
      result.Columns.Add("email");
      result.Columns.Add("mobile");
      result.Columns.Add("prefix");
      result.Columns.Add("extension");
      result.Columns.Add("phoneid");

      try {
        foreach (XmlNode user in response.FirstChild.ChildNodes) {
          DataRow curUser = result.NewRow();

          foreach (XmlNode info in user.ChildNodes) {
            switch (info.Name) {
            case "sop1_ip":
              curUser["sop"] = info.InnerText;
              break;

            case "dp":
              curUser["phoneid"] = info.InnerText;
              break;

            case "mobile":
              if (info.InnerText.StartsWith("00032"))
                curUser["mobile"] = info.InnerText.Replace("00032", "+32");
              else if (info.InnerText.StartsWith("004"))
                curUser["mobile"] = "+32" + info.InnerText.TrimStart('0');
              else
                curUser["mobile"] = info.InnerText;
              break;

            case "fn":
              curUser["firstname"] = info.InnerText;
              break;

            case "ln":
              curUser["lastname"] = info.InnerText;
              break;

            case "ext":
              curUser["extension"] = info.InnerText;
              break;

            case "email":
              curUser["email"] = info.InnerText;
              break;

            case "outgoing_number":
              if (info.InnerText.Length > 3)
                curUser["prefix"] = info.InnerText.Remove(info.InnerText.Length - 3).Replace("00032", "+32");
              else
                curUser["prefix"] = "";
              break;
            }
          }

          result.Rows.Add(curUser);

        }

      }
      catch (Exception ex) {
        string error = "geen gegevens gevonden: " + ex.Message;
      }

      return result;
    }

    public string syncPhoneUsers(ref DataTable sopUsers, ref DataTable secuUsers) {
      string result = "";
      string firstName, lastName, sopName;
      string prefix, mobile, extension;

      startTransaction();
      foreach (DataRow sopUser in sopUsers.Rows) {
        firstName = sopUser["firstname"].ToString();
        lastName = sopUser["lastname"].ToString();
        sopName = sopUser["sop"].ToString();

        prefix = sopUser["prefix"].ToString();
        mobile = sopUser["mobile"].ToString();
        extension = sopUser["extension"].ToString();

        DataRow[] select = secuUsers.Select("lastname='" + lastName + "' and firstname = '" + firstName + "' and sop in ('" + sopName + "', '')");
        string tmpResult = "";
        if (select.Length == 1) {
          tmpResult += updUser(Convert.ToInt32(select[0]["badgeid"]), "sop", sopName);
          tmpResult += updUser(Convert.ToInt32(select[0]["badgeid"]), "extension", prefix + extension);
          tmpResult += updUser(Convert.ToInt32(select[0]["badgeid"]), "mobile", mobile);
          tmpResult += updUser(Convert.ToInt32(select[0]["badgeid"]), "phoneid", sopUser["phoneid"].ToString());

        } else if (select.Length == 0) {
          //string tmp = "niemand gevonden";

          int badgeid = addUser(sopName, firstName, lastName, prefix + extension);
          tmpResult += updUser(badgeid, "mobile", mobile);
          tmpResult += updUser(badgeid, "phoneid", sopUser["phoneid"].ToString());

        } else if (select.Length > 1) {
          string tmp = "meer dan 1 gevonden";
        }

        if (tmpResult != "")
          result += tmpResult + "\r\n";
      }
      commitTransaction();

      return result;
    }
    #endregion

    #region PHONE STATE actions
    public phoneData getPhoneState(string sopIP, string phoneID) {
      phoneData result = new phoneData();

      string error = "";
      string request = createRequest(sopIP, "getPhoneState", phoneID);
      XmlDocument requestResult = doRequest(request, out error);

      if (error == "")
        result = processPhoneState(requestResult);

      return result;
    }

    private phoneData processPhoneState(XmlDocument response) {
      phoneData result = new phoneData();

      foreach (XmlNode info in response.FirstChild.FirstChild.ChildNodes) {
        switch (info.Name) {
        case "callerid":
          result.callerID = info.InnerText.Split('|');
          break;
        case "lastcall":
          result.lastCall = info.InnerText.Split('|');
          break;
        case "state":
          result.state = info.InnerText.Split('|');
          break;
        case "localchannel":
          result.localChannel = info.InnerText.Split('|');
          break;
        case "remotechannel":
          result.remoteChannel = info.InnerText.Split('|');
          break;
        }
      }
      return result;
    }

    #endregion

    #region PHONE CALLING actions
    public bool callInitiate(string sopIP, string phoneID, string calledNumber) {
      bool result = false;

      string error = "";
      string request = createRequest(sopIP, "callInitiate", phoneID + "&to=" + calledNumber);
      XmlDocument requestResult = doRequest(request, out error);

      if (error == "")
        result = true;

      return result;
    }

    public bool callRedirect(string sopIP, string channelID, string calledNumber) {
      bool result = false;

      string error = "";
      string request = createRequest(sopIP, "callRedirect", channelID + "&to=" + calledNumber);
      XmlDocument requestResult = doRequest(request, out error);

      if (error == "")
        result = true;

      return result;
    }

    public bool callPark(string sopIP, string channelID) {
      bool result = false;

      string error = "";
      string request = createRequest(sopIP, "callPark", channelID);
      XmlDocument requestResult = doRequest(request, out error);

      if (error == "")
        result = true;

      return result;
    }

    public List<parkData> getParkedChannels(string sopIP) {
      List<parkData> result = new List<parkData>();

      string error = "";
      string request = createRequest(sopIP, "getParkedChannels", "");
      XmlDocument requestResult = doRequest(request, out error);

      result = processParkedChannels(requestResult);

      return result;
    }

    private List<parkData> processParkedChannels(XmlDocument response) {
      List<parkData> result = new List<parkData>();

      try {
        if (response.ChildNodes[0].ChildNodes.Count == 1)
          return result;

        foreach (XmlNode info in response.ChildNodes[0].ChildNodes) {
          if (info.Name == "Channel") {
            parkData thisChannel = new parkData();
            foreach (XmlNode parkChannel in info.ChildNodes) {
              switch (parkChannel.Name) {
              case "ID":
                thisChannel.channelID = parkChannel.InnerText;
                break;
              case "CallerId":
                thisChannel.callerID = parkChannel.InnerText;
                break;
              case "ParkSlot":
                thisChannel.parkSlot = Convert.ToInt32(parkChannel.InnerText);
                break;
              }   
            }
            result.Add(thisChannel);
          }


        }
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      return result;
    }

    public bool joinPark(string sopIP, string channelID, int parkSlot) {
      bool result = false;

      string error = "";
      string request = createRequest(sopIP, "callJoin", channelID + "&to=" + parkSlot.ToString());
      XmlDocument requestResult = doRequest(request, out error);

      return result;
    }
    #endregion

    #region CAMERA actions
    public MemoryStream camFetchSingle(string camIP, string APIcommand) {
      MemoryStream result = new MemoryStream();

      string error = "";
      try {
        result = doCamRequest(camIP, APIcommand);
      }
      catch (Exception ex) {
        error = ex.Message;
      }

      return result;
    }
    #endregion

    private string processCallflowResult(XmlDocument response) {
      string result = "";
      try {
        foreach (XmlNode info in response.FirstChild.ChildNodes) {
          switch (info.Name) {
          case "CurrentProfile":
            string[] splitProfile = info.InnerText.Split('-');
            userCallflow.callflowUser = info.InnerText;
            userCallflow.callflowType = splitProfile[1];
            userCallflow.callflowColor = splitProfile[2];
            break;

          case "CurrentStatus":
            userStatus = info.InnerText;
            break;

          case "Error":
            result = info.InnerText;
            break;
          }
        }
      }
      catch (Exception ex) {
        result = ex.Message;
      }

      return result;
    }


  }
}
