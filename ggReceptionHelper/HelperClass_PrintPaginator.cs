﻿/*
********************************************************************************** 
THIS NOTICE IS REQUIRED TO BE PRESENT IN ALL COPIES OF THIS SOURCE CODE 
**********************************************************************************

PFSDocumentPaginator (Extends DynamicDocumentPaginator)                                     
Copyright: 2014, Paul F. Sirpenski, (http://www.sirpenski.com)   
Last Modified: 4/21/2014.
 
The PFSDocumentPaginator is free software: you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation, either version 3 of the License, or (at your option) any later version.

The PFSDocumentPaginator is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
details.

You should have received a copy of the GNU General Public License along 
with PFSDcoumentPaginator. If not, see http://www.gnu.org/licenses/.

***********************************************************************************
***********************************************************************************
*/
using System;
using System.Collections;
using System.IO;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Markup;
using System.Xml;
using System.Xaml;
using System.Drawing;
using System.Windows.Media.Imaging;

namespace ggReceptionHelper {

  public class SimplePaginator : DynamicDocumentPaginator {
    const double DOTS_PER_INCH = 96.0;
    const double DOTS_PER_MIL = 96.0 / 25.4;

    #region Simple properties
    public FlowDocument docHeader;
    public FlowDocument docContent;
    public FlowDocument docFooter;

    private ContainerVisual currentHeader;

    public bool repeatTableHeaders;

    private IDocumentPaginatorSource docPaginatorSource;

    private bool allInitialized;
    private bool hasHeader;
    private bool hasFooter;

    public System.Windows.Size pageSize;
    public double headerHeight;
    public double footerHeight;

    public double marginLeft;
    public double marginTop;
    public double marginRight;
    public double marginBottom;

    private double pageHeight;
    private double pageWidth;
    private double colWidth;
    private double colHeight;
    #endregion

    public SimplePaginator() {
      docHeader = null;
      docContent = null;
      docFooter = null;

      repeatTableHeaders = false;

      allInitialized = false;
      hasHeader = false;
      hasFooter = false;

      pageSize = new System.Windows.Size(210 * DOTS_PER_MIL, 290 * DOTS_PER_MIL);
      headerHeight = 75;
      footerHeight = 75;

      marginLeft = 50;
      marginRight = 50;

      marginTop = 50;
      marginBottom = 50;

      //currentHeader = null;
      //  curPageNum = 0;
    }

    #region Simple override routines
    public override bool IsPageCountValid {
      get {
        if (docContent == null)
          throw new InvalidOperationException("document content is not initialized");

        return this.Source.DocumentPaginator.IsPageCountValid;
      }
    }

    public override int PageCount {
      get {
        if (docContent == null)
          throw new InvalidOperationException("document content is not initialized");

        return this.Source.DocumentPaginator.PageCount;
      }
    }

    public override System.Windows.Size PageSize {
      get {
        return this.Source.DocumentPaginator.PageSize;
      }
      set {
        this.Source.DocumentPaginator.PageSize = value;
      }
    }

    public override IDocumentPaginatorSource Source {
      get {
        return docPaginatorSource;
      }
    }

    public override ContentPosition GetObjectPosition(object value) {
      throw new NotImplementedException();
    }

    public override DocumentPage GetPage(int pageNumber) {

      if (docContent == null)
        throw new InvalidOperationException("no document to print");

      ContainerVisual headerCV = new ContainerVisual();
      ContainerVisual contentCV = new ContainerVisual();
      ContainerVisual footerCV = new ContainerVisual();
      ContainerVisual pageCV = new ContainerVisual();

      if (!allInitialized) {
        initDocumentProperties();
        allInitialized = true;

        this.Source.DocumentPaginator.ComputePageCount();
      }

      //pageCV.Children.Capacity = 3;
      DocumentPage printPage = this.Source.DocumentPaginator.GetPage(pageNumber);

      if (hasHeader) {
        IDocumentPaginatorSource headerSource = (IDocumentPaginatorSource)docHeader;

        //headerSource.DocumentPaginator.PageSize = new System.Windows.Size(this.Source.DocumentPaginator.PageSize.Width, headerHeight);
        headerSource.DocumentPaginator.PageSize = new System.Windows.Size(colWidth, headerHeight);
        DocumentPage headerDP = headerSource.DocumentPaginator.GetPage(0);

        headerCV = (ContainerVisual)headerDP.Visual;
        headerCV.Transform = new TranslateTransform(0, marginTop);
        pageCV.Children.Add(headerCV);

      }

      if (hasFooter) {
        IDocumentPaginatorSource footerSource = (IDocumentPaginatorSource)docFooter;

        //footerSource.DocumentPaginator.PageSize = new System.Windows.Size(this.Source.DocumentPaginator.PageSize.Width, footerHeight);
        footerSource.DocumentPaginator.PageSize = new System.Windows.Size(colWidth, footerHeight);
        DocumentPage footerDP = footerSource.DocumentPaginator.GetPage(0);

        footerCV = (ContainerVisual)footerDP.Visual;
        footerCV.Transform = new TranslateTransform(0, pageHeight - footerHeight - marginBottom);
        pageCV.Children.Add(footerCV);
      }

      contentCV = (ContainerVisual)printPage.Visual;
      contentCV.Transform = new TranslateTransform(0, headerHeight + marginTop);
      pageCV.Children.Add(contentCV);

#if DEBUG
      if (repeatTableHeaders) {
        ContainerVisual table;
        if (PageStartsWithTable(contentCV, out table) && currentHeader != null) {
          Rect headerBounds = VisualTreeHelper.GetDescendantBounds(currentHeader);
          Vector offset = VisualTreeHelper.GetOffset(currentHeader);
          ContainerVisual headerVisual = new ContainerVisual();

          headerVisual.Transform = new TranslateTransform(0, headerHeight + marginTop);

          double yscale = colHeight / colHeight;
          TransformGroup group = new TransformGroup();
          group.Children.Add(new ScaleTransform(1.0, yscale));
          group.Children.Add(new TranslateTransform(0, headerHeight + marginTop));
          contentCV.Transform = group;

          ContainerVisual cp = VisualTreeHelper.GetParent(currentHeader) as ContainerVisual;
          if (cp != null) {
            cp.Children.Remove(currentHeader);
          }
          headerVisual.Children.Add(currentHeader);
          pageCV.Children.Add(headerVisual);
        }

        ContainerVisual newTable, newHeader;
        if (PageEndsWithTable(contentCV, out newTable, out newHeader)) {
          if (newTable == table) {
            // Still the same table so don't change the repeating header
          } else {
            // We've found a new table. Repeat the header on the next page
            currentHeader = newHeader;
          }
        } else {
          // There was no table at the end of the page
          currentHeader = null;
        }
      }
#endif

      Rect bleedBox = new Rect(pageSize);
      Rect contentBox = new Rect(pageSize);
      DocumentPage dp = new DocumentPage(pageCV, PageSize, bleedBox, contentBox);

      // now get the visual of the new document page
      ContainerVisual dpVisual = (ContainerVisual)dp.Visual;

      // perform a final translation on the new page moving it to the left 
      // i.e. left gutter plus left margin
      dpVisual.Transform = new TranslateTransform(marginLeft, 0);

      return dp;
    }

    public override int GetPageNumber(ContentPosition contentPosition) {
      throw new NotImplementedException();
    }

    public override ContentPosition GetPagePosition(DocumentPage page) {
      throw new NotImplementedException();
    }
    #endregion



    #region Simple content assigning
    public void SetHeader(FlowDocument input) {
      docHeader = input;

      if (!double.IsNaN(docHeader.PageHeight))
        headerHeight = docHeader.PageHeight;
    }
    public void SetContent(FlowDocument input) {
      docContent = input;
      docPaginatorSource = (IDocumentPaginatorSource)docContent;

      if (!double.IsNaN(docContent.PageHeight)) {
        pageHeight = docContent.PageHeight;
      }
      if (!double.IsNaN(docContent.PageWidth)) {
        pageWidth = docContent.PageWidth;
      }
    }
    public void SetFooter(FlowDocument input) {
      docFooter = input;

      if (!double.IsNaN(docFooter.PageHeight))
        footerHeight = docFooter.PageHeight;
    }
    #endregion

#if DEBUG
    private ImageBrush addBorders(int width, int height, System.Drawing.Pen color) {
      Bitmap img = new Bitmap(width, height);
      Graphics bg = Graphics.FromImage(img);

      bg.DrawRectangle(color, 0, 0, width, height);

      ImageBrush result = new ImageBrush(Imaging.CreateBitmapSourceFromHBitmap(img.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions()));

      result.Viewbox = new Rect(0, 0, width, height);
      result.ViewboxUnits = BrushMappingMode.Absolute;
      result.Viewport = new Rect(0, 0, width, height);
      result.ViewportUnits = BrushMappingMode.Absolute;
      result.Stretch = Stretch.Uniform;
      result.TileMode = TileMode.None;
      result.AlignmentX = AlignmentX.Center;
      result.AlignmentY = AlignmentY.Center;
      result.Freeze();

      return result;
    }

    private bool PageEndsWithTable(DependencyObject element, out ContainerVisual tableVisual, out ContainerVisual headerVisual) {
      tableVisual = null;
      headerVisual = null;
      if (element.GetType().Name == "RowVisual") {
        tableVisual = (ContainerVisual)VisualTreeHelper.GetParent(element);
        headerVisual = (ContainerVisual)VisualTreeHelper.GetChild(tableVisual, 0);

        var tmp = tableVisual.GetType();
        return true;
      }
      int children = VisualTreeHelper.GetChildrenCount(element);
      if (element.GetType() == typeof(ContainerVisual)) {
        for (int c = children - 1; c >= 0; c--) {
          DependencyObject child = VisualTreeHelper.GetChild(element, c);
          if (PageEndsWithTable(child, out tableVisual, out headerVisual)) {
            return true;
          }
        }
      } else if (children > 0) {
        DependencyObject child = VisualTreeHelper.GetChild(element, children - 1);
        if (PageEndsWithTable(child, out tableVisual, out headerVisual)) {
          return true;
        }
      }
      return false;

      //   test
    }

    private bool PageStartsWithTable(DependencyObject element, out ContainerVisual tableVisual) {
      tableVisual = null;
      if (element.GetType().Name == "RowVisual") {
        tableVisual = (ContainerVisual)VisualTreeHelper.GetParent(element);
        return true;
      }
      if (VisualTreeHelper.GetChildrenCount(element) > 0) {
        DependencyObject child = VisualTreeHelper.GetChild(element, 0);
        if (PageStartsWithTable(child, out tableVisual)) {
          return true;
        }
      }
      return false;
    }


#endif

    private void initDocumentProperties() {
      if (docHeader != null) {
        hasHeader = true;
      }

      if (docFooter != null) {
        hasFooter = true;
      }

      pageHeight = pageSize.Height;
      pageWidth = pageSize.Width;
      colWidth = pageWidth - marginLeft - marginRight;
      colHeight = pageHeight - headerHeight - footerHeight - marginTop - marginBottom;

      if (hasHeader) {
        docHeader.PageWidth = pageWidth;
        docHeader.PageHeight = headerHeight;
        docHeader.ColumnWidth = colWidth;
        docHeader.IsColumnWidthFlexible = false;

#if DEBUG
        docHeader.Background = addBorders(Convert.ToInt32(colWidth), Convert.ToInt32(headerHeight), Pens.Red);
#endif
      }

      docContent.PageHeight = colHeight;
      docContent.PageWidth = colWidth;
      docContent.ColumnWidth = colWidth;
      docContent.IsColumnWidthFlexible = false;
      docContent.PagePadding = new Thickness(0);

#if DEBUG
      docContent.Background = addBorders(Convert.ToInt32(colWidth), Convert.ToInt32(docContent.PageHeight), Pens.Green);
#endif

      if (hasFooter) {
        docFooter.PageWidth = pageWidth;
        docFooter.PageHeight = footerHeight;
        docFooter.ColumnWidth = colWidth;
        docFooter.IsColumnWidthFlexible = false;

#if DEBUG
        docFooter.Background = addBorders(Convert.ToInt32(colWidth), Convert.ToInt32(footerHeight), Pens.Blue);
#endif
      }
    }
  }

    


#if _PSFPAGINATOR_
  public class PFSDocumentPaginator : DynamicDocumentPaginator {

#region PSF constantes
    const double DOTS_PER_INCH = 96.0;

    // document field name constants.  These should be inserted
    // into header footer fields
    const string DOCUMENT_FIELD_NAME_PREFIX = "${";                     // beginning of proerty name field.
    const string DOCUMENT_FIELD_NAME_SUFFIX = "}";                      // end of property name field

    // document property field names.  Insert in header and 
    // footer Ex. ${pg} will print the current page number.
    const string DOCUMENT_FIELD_NAME_DATE_SHORT = "dateshort";          // short date format as defined by computer
    const string DOCUMENT_FIELD_NAME_DATE_LONG = "datelong";            // long date format as defined by computer
    const string DOCUMENT_FIELD_NAME_DATE_MDY = "datemdy";              // date mm/dd/yyyy
    const string DOCUMENT_FIELD_NAME_DATE_YMD = "dateymd";              // date in format yyyy-mm-dd
    const string DOCUMENT_FIELD_NAME_DATE_DMY = "datedmy";              // date in format dd/mm/yyyy
    const string DOCUMENT_FIELD_NAME_TIME_HM_AMPM = "timehmampm";       // time in formation hh:mm AM/PM
    const string DOCUMENT_FIELD_NAME_TIME_HM = "timehm";                // time in 24 hour format hh:mm
    const string DOCUMENT_FIELD_NAME_TIME_HMS = "timehms";              // time in 24 hour format hh:mm:ss
    const string DOCUMENT_FIELD_NAME_DATETIME_LONG = "datetimelong";    // long date time
    const string DOCUMENT_FIELD_NAME_DATETIME_SHORT = "datetimeshort";  // short date time
    const string DOCUMENT_FIELD_NAME_PAGENUMBER = "pg";                 // page number
    const string DOCUMENT_FIELD_NAME_NUMPAGES = "numpages";             // number of total pages
    const string DOCUMENT_FIELD_NAME_TITLE = "title";                   // document title - settable by user
    const string DOCUMENT_FIELD_NAME_USER_PRINTDATE = "usrprintdate";   // document print date - settable by user
    const string DOCUMENT_FIELD_NAME_FILENAME = "fn";                   // filename


    // Error Strings.  Change based upon desired language.
    const string ERR_INPUT_HEADER_FLOW_DOCUMENT_FILE = "Error In Header FlowDocument File.";
    const string ERR_INPUT_FOOTER_FLOW_DOCUMENT_FILE = "Error In Footer FlowDocument File.";
    const string ERR_INPUT_CONTENT_FLOW_DOCUMENT_FILE = "Error In Content FlowDocument File.";
    const string ERR_NO_CONTENT_FLOW_DOCUMENT_SPECIFIED = "Error.  No Content Flow Document Specified.";
    const string ERR_PAGE_WIDTH = "Error In Left And Right Margins.  They Exceed Page Width";
    const string ERR_PAGE_HEIGHT = "Error in Margin, Footer, And Header Sizes.  They Exceed Page Height";
    const string ERR_ODD_PAGE_HEADER_DEFINED_EVEN_PAGE_HEADER_NOT = "Error.  Odd Page Header Is Specified. Even Page or Normal Header Is Not Specified.";
    const string ERR_EVEN_PAGE_HEADER_DEFINED_ODD_PAGE_HEADER_NOT = "Error. Even Page Header Is Specified. Odd Page or Normal Header Is Not Specified.";
    const string ERR_FIRST_PAGE_HEADER_DEFINED_ODD_PAGE_HEADER_NOT = "Error.  First Page Header Is Specified.  Odd Page Or Normal Header Is Not Specified.";
    const string ERR_ODD_PAGE_FOOTER_DEFINED_EVEN_PAGE_FOOTER_NOT = "Error.  Odd Page Footer Is Specified.  Even Page Or Normal Footer Is Not Specified.";
    const string ERR_EVEN_PAGE_FOOTER_DEFINED_ODD_PAGE_FOOTER_NOT = "Error. Even Page Footer Is Specified.  Odd Page Or Normal Footer Is Not Specified.";
    const string ERR_FIRST_PAGE_FOOTER_DEFINED_ODD_PAGE_FOOTER_NOT = "Error. First Page Footer Is Specified.  Odd Page Or Normal Footer Is Not Specified.";
#endregion

#region PSF variables
    private ArrayList DocumentProperties = new ArrayList();             // holds all the document properties

    private double dblMarginLeft = 0;                                   // margin left in unit of measure
    private double dblMarginRight = 0;                                  // margin right in unit of measure
    private double dblMarginBottom = 0;                                 // margin bottom in unit of measure
    private double dblMarginTop = 0;                                    // margin top in unit of measure

    private double dblFooterHeight = 0;                                 // footer height in unit of measure
    private double dblHeaderHeight = 0;                                 // header height in unit of measure
    private double dblPageHeight = 0;                                   // page height of content in unit of measure
    private double dblPageWidth = 0;                                    // page width of content in unit of measure

    private double dblGutterTop = 0;                                    // Gutter Top
    private double dblGutterBottom = 0;                                 // Gutter Bottom
    private double dblGutterRight = 0;                                  // Gutter Right
    private double dblGutterLeft = 0;                                   // Gutter Left


    private Size szPageSize;                                            // page size of content
    private Size szHeaderSize;                                          // header size
    private Size szFooterSize;                                          // footer size

    private string strPropertyNamePrefix = "";                          // property name prefix 
    private string strPropertyNameSuffix = "";                          // property name suffix - used to end property name definition
    private string strDocumentTitle = "";                               // convenience property document title
    private string strDocumentUserPrintDate = "";                         // convenience property document print date - set by user
    private string strDocumentFileName = "";                            // filename


    private IDocumentPaginatorSource objPaginatorSource = null;         // document paginator source
    private FlowDocument objContentFlowDocument = null;                 // flow document passed in

    private FlowDocument objHeaderFlowDocument = null;                  // header flow document passed in
    private FlowDocument objHeaderFlowDocumentFirstPage = null;         // header first page flow document
    private FlowDocument objHeaderFlowDocumentOddPage = null;           // header odd page flow document
    private FlowDocument objHeaderFlowDocumentEvenPage = null;         // header even page flow document

    private FlowDocument objFooterFlowDocument = null;                  // footer flow document passed in
    private FlowDocument objFooterFlowDocumentFirstPage = null;         // Footer first page flow document
    private FlowDocument objFooterFlowDocumentOddPage = null;           // Footer odd page flow document
    private FlowDocument objFooterFlowDocumentEvenPage = null;         // Footer even page flow document


    private Table tblHeader = null;                                     // header table passed in
    private Table tblHeaderFirstPage = null;                            // header table first page
    private Table tblHeaderOddPage = null;                              // header table odd page
    private Table tblHeaderEvenPage = null;                             // header table even page

    private Table tblFooter = null;                                     // footer table passed in
    private Table tblFooterFirstPage = null;                            // Footer table first page
    private Table tblFooterOddPage = null;                              // Footer table odd page
    private Table tblFooterEvenPage = null;                             // Footer table even page

    private bool boolProcessHeader = false;                             // a flag indicating whether to process the header.  Used by GetPage
    private bool boolProcessFooter = false;                             // a flag indicating whether to process the footer.  Used by GetPage
    private bool boolSystemIsInitialized = false;                       // a flag indicating whether the system is initialized or not.
                                                                        // i.e. the GetPage method was ever called.

#endregion

#region PSF Properties
    // ----------------------------------------------
    // General Properties
    // ---------------------------------------------- 
    public string PropertyNamePrefix {
      get { return strPropertyNamePrefix; }
      set { strPropertyNamePrefix = value; }
    }

    public string PropertyNameSuffix {
      get { return strPropertyNameSuffix; }
      set { strPropertyNameSuffix = value; }
    }

    public string DocumentTitle {
      get { return strDocumentTitle; }
      set { strDocumentTitle = value; }
    }

    public string DocumentUserPrintDate {
      get { return strDocumentUserPrintDate; }
      set { strDocumentUserPrintDate = value; }
    }

    public string DocumentFileName {
      get { return strDocumentFileName; }
      set { strDocumentFileName = value; }
    }

    public double PageHeight {
      get { return dblPageHeight; }
      set {
        dblPageHeight = value;
      }
    }

    public double PageWidth {
      get { return dblPageWidth; }
      set {
        dblPageWidth = value;
      }
    }

    public double GutterLeft {
      get { return dblGutterLeft; }
      set {
        dblGutterLeft = value;
        SetSizes();
      }
    }

    public double GutterRight {
      get { return dblGutterRight; }
      set {
        dblGutterRight = value;

      }
    }

    public double GutterTop {
      get { return dblGutterTop; }
      set {
        dblGutterTop = value;
      }
    }

    public double GutterBottom {
      get { return dblGutterBottom; }
      set {
        dblGutterBottom = value;
      }
    }

    public double MarginLeft {
      get { return dblMarginLeft; }
      set {
        dblMarginLeft = value;
        SetSizes();
      }
    }

    public double MarginRight {
      get { return dblMarginRight; }
      set {
        dblMarginRight = value;

      }
    }

    public double MarginTop {
      get { return dblMarginTop; }
      set {
        dblMarginTop = value;
      }
    }

    public double MarginBottom {
      get { return dblMarginBottom; }
      set {
        dblMarginBottom = value;
      }
    }

    public double FooterHeight {
      get { return dblFooterHeight; }
      set {
        dblFooterHeight = value;
      }
    }

    public double HeaderHeight {
      get { return dblHeaderHeight; }
      set {
        dblHeaderHeight = value;
      }
    }

    public double PageWidthInches {
      get { return dblPageWidth / DOTS_PER_INCH; }
      set { this.PageWidth = value * DOTS_PER_INCH; }
    }

    public double PageHeightInches {
      get { return dblPageHeight / DOTS_PER_INCH; }
      set { this.PageHeight = value * DOTS_PER_INCH; }
    }

    public double HeaderHeightInches {
      get { return dblHeaderHeight / DOTS_PER_INCH; }
      set { this.HeaderHeight = value * DOTS_PER_INCH; }
    }

    public double FooterHeightInches {
      get { return dblFooterHeight / DOTS_PER_INCH; }
      set { this.FooterHeight = value * DOTS_PER_INCH; }
    }


    public double GutterTopInches {
      get { return dblGutterTop / DOTS_PER_INCH; }
      set { this.GutterTop = value * DOTS_PER_INCH; }
    }

    public double GutterBottomInches {
      get { return dblGutterBottom / DOTS_PER_INCH; }
      set { this.GutterBottom = value * DOTS_PER_INCH; }
    }

    public double GutterLeftInches {
      get { return dblGutterLeft / DOTS_PER_INCH; }
      set { this.GutterLeft = value * DOTS_PER_INCH; }
    }

    public double GutterRightInches {
      get { return dblGutterRight / DOTS_PER_INCH; }
      set { dblGutterRight = value * DOTS_PER_INCH; }
    }


    public double MarginTopInches {
      get { return dblMarginTop / DOTS_PER_INCH; }
      set { this.MarginTop = value * DOTS_PER_INCH; }
    }

    public double MarginBottomInches {
      get { return dblMarginBottom / DOTS_PER_INCH; }
      set { this.MarginBottom = value * DOTS_PER_INCH; }
    }

    public double MarginLeftInches {
      get { return dblMarginLeft / DOTS_PER_INCH; }
      set { this.MarginLeft = value * DOTS_PER_INCH; }
    }

    public double MarginRightInches {
      get { return dblMarginRight / DOTS_PER_INCH; }
      set { dblMarginRight = value * DOTS_PER_INCH; }
    }
#endregion

    // --------------------------------------------------------------
    // This is the constructor
    // --------------------------------------------------------------
    public PFSDocumentPaginator() {

      // set default document property name prefix and suffix
      strPropertyNamePrefix = DOCUMENT_FIELD_NAME_PREFIX;
      strPropertyNameSuffix = DOCUMENT_FIELD_NAME_SUFFIX;

      // set the default document sizes
      dblPageHeight = 11.0 * DOTS_PER_INCH;       // 11 inches high.
      dblPageWidth = 8.5 * DOTS_PER_INCH;         // 8 1/2 inches wide.
      dblMarginTop = 0.5 * DOTS_PER_INCH;         // 1/2 inch.
      dblMarginBottom = 0.5 * DOTS_PER_INCH;      // 1/2 inch.
      dblMarginLeft = 0.75 * DOTS_PER_INCH;        // 1/2 inch.
      dblMarginRight = 0.75 * DOTS_PER_INCH;       // 1/2 inch.
      dblFooterHeight = 0.5 * DOTS_PER_INCH;      // 1/2 inch.
      dblHeaderHeight = 0.5 * DOTS_PER_INCH;      // 1/2 inch.

      dblGutterTop = 0 * DOTS_PER_INCH;           // 0 inch.
      dblGutterBottom = 0 * DOTS_PER_INCH;        // 0 inch.
      dblGutterLeft = 0 * DOTS_PER_INCH;          // 0 inch.
      dblGutterRight = 0 * DOTS_PER_INCH;         // 0 inch.

      // now do a compute to compute the class level fields
      SetSizes();

    }

    // ----------------------------------------------------------------------
    // Initializes Document Properties.  Called By GetPage Initialization.  
    // This routine is called after total pages is completed.
    // ----------------------------------------------------------------------
    private void InitializeDocumentProperties() {
      DateTime dt = DateTime.Now;

      InitProperty(DOCUMENT_FIELD_NAME_PAGENUMBER, "");                                // page number
      InitProperty(DOCUMENT_FIELD_NAME_NUMPAGES, this.PageCount.ToString());           // number of total pages
      InitProperty(DOCUMENT_FIELD_NAME_TITLE, strDocumentTitle);                       // document title set by user
      InitProperty(DOCUMENT_FIELD_NAME_USER_PRINTDATE, strDocumentUserPrintDate);     // document print date set by user
      InitProperty(DOCUMENT_FIELD_NAME_FILENAME, strDocumentFileName);                // documet file name

      InitProperty(DOCUMENT_FIELD_NAME_DATE_SHORT, dt.ToString("d"));
      InitProperty(DOCUMENT_FIELD_NAME_DATE_LONG, dt.ToString("D"));
      InitProperty(DOCUMENT_FIELD_NAME_DATE_MDY, dt.ToString("MM/dd/yyyy"));
      InitProperty(DOCUMENT_FIELD_NAME_DATE_YMD, dt.ToString("yyyy-MM-dd"));
      InitProperty(DOCUMENT_FIELD_NAME_DATE_YMD, dt.ToString("dd/MM/yyyy"));

      InitProperty(DOCUMENT_FIELD_NAME_TIME_HM_AMPM, dt.ToString("hh:mm tt"));
      InitProperty(DOCUMENT_FIELD_NAME_TIME_HM, dt.ToString("HH:mm"));
      InitProperty(DOCUMENT_FIELD_NAME_TIME_HMS, dt.ToString("HH:mm:ss"));
      InitProperty(DOCUMENT_FIELD_NAME_DATETIME_LONG, dt.ToString("G"));
      InitProperty(DOCUMENT_FIELD_NAME_DATETIME_SHORT, dt.ToString("g"));

    }

    // ----------------------------------------------------------------------
    // This is a helper routine to initialize the document properties which 
    // is done in the class constructor
    // ----------------------------------------------------------------------
    private void InitProperty(string propertyBaseName, string propertyValue) {
      PFSDocumentProperty p = new PFSDocumentProperty();
      p.Name = GetFullPropertyName(propertyBaseName);
      p.Value = propertyValue;
      DocumentProperties.Add(p);
    }

    // --------------------------------------------------------------------
    // Computes the full Property Name
    // --------------------------------------------------------------------
    private string GetFullPropertyName(string propertyBaseName) {
      return strPropertyNamePrefix + propertyBaseName + strPropertyNameSuffix;
    }

#region PSF Header routines
    // --------------------------------------------------------------
    // sets the header table
    // --------------------------------------------------------------
    public void SetHeader(Table t) {
      tblHeader = t;
    }

    // --------------------------------------------------------------
    // Set Header From Flow Document Object
    // --------------------------------------------------------------
    public void SetHeader(FlowDocument f) {
      objHeaderFlowDocument = f;

      if (!double.IsNaN(f.PageHeight))
        this.HeaderHeight = f.PageHeight;
    }


    // --------------------------------------------------------------
    // Sets Header From Flow Document File
    // --------------------------------------------------------------
    public void SetHeaderFromFile(string fileName) {
      FlowDocument f;
      try {
        f = LoadFlowDocumentFromFile(fileName);
        this.SetHeader(f);
      }
      catch {
        throw new System.InvalidOperationException(ERR_INPUT_HEADER_FLOW_DOCUMENT_FILE);
      }
    }


    // --------------------------------------------------------------
    // sets the header first page table
    // --------------------------------------------------------------
    public void SetHeaderFirstPage(Table t) {
      tblHeaderFirstPage = t;
    }

    // --------------------------------------------------------------
    // Set First Page Header From Flow Document Object
    // --------------------------------------------------------------
    public void SetHeaderFirstPage(FlowDocument f) {
      objHeaderFlowDocumentFirstPage = f;

    }


    // --------------------------------------------------------------
    // Sets Header First Page From Flow Document File
    // --------------------------------------------------------------
    public void SetHeaderFirstPageFromFile(string fileName) {
      FlowDocument f;
      try {
        f = LoadFlowDocumentFromFile(fileName);
        this.SetHeaderFirstPage(f);
      }
      catch {
        throw new System.InvalidOperationException(ERR_INPUT_HEADER_FLOW_DOCUMENT_FILE);
      }
    }


    // --------------------------------------------------------------
    // sets the header odd page table
    // --------------------------------------------------------------
    public void SetHeaderOddPage(Table t) {
      tblHeaderOddPage = t;
    }

    // --------------------------------------------------------------
    // Set Odd Page Header From Flow Document Object
    // --------------------------------------------------------------
    public void SetHeaderOddPage(FlowDocument f) {
      objHeaderFlowDocumentOddPage = f;

    }


    // --------------------------------------------------------------
    // Sets Header Odd Page From Flow Document File
    // --------------------------------------------------------------
    public void SetHeaderOddPageFromFile(string fileName) {
      FlowDocument f;
      try {
        f = LoadFlowDocumentFromFile(fileName);
        this.SetHeaderOddPage(f);
      }
      catch {
        throw new System.InvalidOperationException(ERR_INPUT_HEADER_FLOW_DOCUMENT_FILE);
      }
    }

    // --------------------------------------------------------------
    // sets the header Even page table
    // --------------------------------------------------------------
    public void SetHeaderEvenPage(Table t) {
      tblHeaderEvenPage = t;
    }

    // --------------------------------------------------------------
    // Set Even Page Header From Flow Document Object
    // --------------------------------------------------------------
    public void SetHeaderEvenPage(FlowDocument f) {
      objHeaderFlowDocumentEvenPage = f;

    }


    // --------------------------------------------------------------
    // Sets Header Even Page From Flow Document File
    // --------------------------------------------------------------
    public void SetHeaderEvenPageFromFile(string fileName) {
      FlowDocument f;
      try {
        f = LoadFlowDocumentFromFile(fileName);
        this.SetHeaderEvenPage(f);
      }
      catch {
        throw new System.InvalidOperationException(ERR_INPUT_HEADER_FLOW_DOCUMENT_FILE);
      }
    }
#endregion

#region PSF footer routines
    // --------------------------------------------------------------
    // sets the footer table
    // --------------------------------------------------------------
    public void SetFooter(Table t) {
      tblFooter = t;
    }

    // --------------------------------------------------------------
    // Set Footer From FlowDocument Object
    // --------------------------------------------------------------
    public void SetFooter(FlowDocument f) {
      objFooterFlowDocument = f;
      if (!double.IsNaN(f.PageHeight))
        this.FooterHeight = f.PageHeight;
    }


    // ---------------------------------------------------------------
    // Set Footer From Flow Document File
    // ---------------------------------------------------------------
    public void SetFooterFromFile(string fileName) {
      FlowDocument f;
      try {
        f = LoadFlowDocumentFromFile(fileName);
        this.SetFooter(f);
      }
      catch {
        throw new System.InvalidOperationException(ERR_INPUT_FOOTER_FLOW_DOCUMENT_FILE);
      }
    }


    // --------------------------------------------------------------
    // sets the footer first page table
    // --------------------------------------------------------------
    public void SetFooterFirstPage(Table t) {
      tblFooterFirstPage = t;
    }

    // --------------------------------------------------------------
    // Set First Page Footer From FlowDocument Object
    // --------------------------------------------------------------
    public void SetFooterFirstPage(FlowDocument f) {
      objFooterFlowDocumentFirstPage = f;
    }


    // ---------------------------------------------------------------
    // Set Footer First Page From Flow Document File
    // ---------------------------------------------------------------
    public void SetFooterFirstPageFromFile(string fileName) {
      FlowDocument f;
      try {
        f = LoadFlowDocumentFromFile(fileName);
        this.SetFooterFirstPage(f);
      }
      catch {
        throw new System.InvalidOperationException(ERR_INPUT_FOOTER_FLOW_DOCUMENT_FILE);
      }
    }


    // --------------------------------------------------------------
    // sets the footer odd page table
    // --------------------------------------------------------------
    public void SetFooterOddPage(Table t) {
      tblFooterOddPage = t;
    }

    // --------------------------------------------------------------
    // Set Odd Page Footer From FlowDocument Object
    // --------------------------------------------------------------
    public void SetFooterOddPage(FlowDocument f) {
      objFooterFlowDocumentOddPage = f;
    }


    // ---------------------------------------------------------------
    // Set Footer Odd Page From Flow Document File
    // ---------------------------------------------------------------
    public void SetFooterOddPageFromFile(string fileName) {
      FlowDocument f;
      try {
        f = LoadFlowDocumentFromFile(fileName);
        this.SetFooterOddPage(f);
      }
      catch {
        throw new System.InvalidOperationException(ERR_INPUT_FOOTER_FLOW_DOCUMENT_FILE);
      }
    }


    // --------------------------------------------------------------
    // sets the footer Even page table
    // --------------------------------------------------------------
    public void SetFooterEvenPage(Table t) {
      tblFooterEvenPage = t;
    }

    // --------------------------------------------------------------
    // Set Even Page Footer From FlowDocument Object
    // --------------------------------------------------------------
    public void SetFooterEvenPage(FlowDocument f) {
      objFooterFlowDocumentEvenPage = f;
    }


    // ---------------------------------------------------------------
    // Set Footer Even Page From Flow Document File
    // ---------------------------------------------------------------
    public void SetFooterEvenPageFromFile(string fileName) {
      FlowDocument f;
      try {
        f = LoadFlowDocumentFromFile(fileName);
        this.SetFooterEvenPage(f);
      }
      catch {
        throw new System.InvalidOperationException(ERR_INPUT_FOOTER_FLOW_DOCUMENT_FILE);
      }
    }
#endregion

    // ---------------------------------------------------------------
    // This allows the adding of custom document properties
    // ---------------------------------------------------------------
    public void AddDocumentProperty(string DocumentPropertyName, string DocumentPropertyValue) {
      InitProperty(DocumentPropertyName, DocumentPropertyValue);
    }

    // --------------------------------------------------------------
    // sets the input flow document
    // --------------------------------------------------------------
    public void SetContent(FlowDocument fd) {

      objContentFlowDocument = fd;
      objPaginatorSource = (IDocumentPaginatorSource)objContentFlowDocument;
    }

    // --------------------------------------------------------------
    // Sets Content Flow Docuemnt From File
    // ------------------------------------------------------------ 
    public void SetContentFromFile(string fileName) {
      FlowDocument f = new FlowDocument();

      try {
        f = LoadFlowDocumentFromFile(fileName);
        SetContent(f);
      }
      catch {
        throw new System.InvalidOperationException(ERR_INPUT_CONTENT_FLOW_DOCUMENT_FILE);
      }
    }

    // ---------------------------------------------------
    // override of DocumentPaginator Class
    // ---------------------------------------------------
    public override IDocumentPaginatorSource Source {
      get { return objPaginatorSource; }

    }

    // ---------------------------------------------------
    // override of DocumentPaginator Class
    // ---------------------------------------------------
    public override bool IsPageCountValid {
      get {
        if (objContentFlowDocument == null) {
          throw new System.InvalidOperationException(ERR_NO_CONTENT_FLOW_DOCUMENT_SPECIFIED);
        }
        return this.Source.DocumentPaginator.IsPageCountValid;
      }
    }


    // ---------------------------------------------------
    // override of DocumentPaginator Class
    // ---------------------------------------------------
    public override int PageCount {
      get {
        if (objContentFlowDocument == null) {
          throw new System.InvalidOperationException(ERR_NO_CONTENT_FLOW_DOCUMENT_SPECIFIED);
        }
        return this.Source.DocumentPaginator.PageCount;
      }
    }

    // ---------------------------------------------------
    // override of DocumentPaginator Class
    // ---------------------------------------------------
    public override Size PageSize {
      get { return this.Source.DocumentPaginator.PageSize; }
      set { this.Source.DocumentPaginator.PageSize = value; }
    }

    // -----------------------------------------------------------
    // override the GetObjectPosition method function of the DynamicDocumentPaginator
    // -----------------------------------------------------------
    public override ContentPosition GetObjectPosition(object value) {
      throw new NotImplementedException();
    }

    // -----------------------------------------------------------
    // override the GetPageNumber method of the DynamicDocumentPaginator
    // -----------------------------------------------------------
    public override int GetPageNumber(ContentPosition contentPosition) {
      throw new NotImplementedException();
    }

    // ------------------------------------------------------------
    // override the GetPagePosition method of the DynamicDocumentPaginator
    // ------------------------------------------------------------
    public override ContentPosition GetPagePosition(DocumentPage page) {
      throw new NotImplementedException();
    }


    // ---------------------------------------------------------------
    // support utility to load an flow document file from disk
    // ---------------------------------------------------------------
    private FlowDocument LoadFlowDocumentFromFile(string sFileName) {
      XmlDocument xdoc = new XmlDocument();
      FlowDocument fdoc = new FlowDocument();

      // load the xml file
      try {
        //  load the xml file
        xdoc.Load(sFileName);

        // get the xml from document as a string
        string s = xdoc.OuterXml;


        // create a string reader using the contents of the xml string
        StringReader stringReader = new StringReader(s);

        // create an xml reader from the xml string contained in the string reader
        XmlReader xmlReader = System.Xml.XmlReader.Create(stringReader);

        // load the xaml reader using the xml reader and create a flowDocument 
        fdoc = (FlowDocument)XamlReader.Load(xmlReader);
      }
      catch {
        throw new System.InvalidOperationException();
      }

      return fdoc;

    }


    // ----------------------------------------------------------------
    // This is simply a private routine called by GetPage Initialization 
    // branch to fix up improperly defined margins, gutters, etc.  
    // Function exists only to keep code readable
    // -----------------------------------------------------------------
    private void FixMarginsAndGutters() {

      if (dblMarginTop < 0 || dblMarginTop > dblPageHeight)
        dblMarginTop = 0;

      if (dblMarginBottom < 0 || dblMarginBottom > dblPageHeight)
        dblMarginBottom = 0;

      if (dblMarginLeft < 0 || dblMarginLeft > dblPageWidth)
        dblMarginLeft = 0;

      if (dblMarginRight < 0 || dblMarginRight > dblPageWidth)
        dblMarginRight = 0;

      if (dblGutterTop < 0 || dblGutterTop > dblPageHeight)
        dblGutterTop = 0;

      if (dblGutterBottom < 0 || dblGutterBottom > dblPageHeight)
        dblGutterBottom = 0;

      if (dblGutterLeft < 0 || dblMarginLeft > dblPageWidth)
        dblGutterLeft = 0;

      if (dblGutterRight < 0 || dblMarginRight > dblPageWidth)
        dblGutterRight = 0;

    }

    // ---------------------------------------------------------------
    //  this computes the sizes and sets the paginator page size.
    //  Note, We'll catch any exceptions during actual GetPage routine 
    //  because sizes can be reset depending upon whether footer, header, etc
    // is present.
    // ---------------------------------------------------------------
    private void SetSizes() {
      double pPageWidth = dblPageWidth - dblMarginLeft - dblMarginRight - dblGutterLeft - dblGutterRight;
      double pPageHeight = dblPageHeight - dblMarginTop - dblMarginBottom - dblFooterHeight - dblHeaderHeight - dblGutterTop - dblGutterBottom;

      double pNewPageWidth = dblPageWidth - dblMarginLeft - dblMarginRight - dblGutterLeft - dblGutterRight;
      double pNewPageHeight = dblPageHeight - dblGutterTop - dblGutterBottom - dblMarginTop - dblMarginBottom;

      szPageSize = new Size(pPageWidth, pPageHeight);
      szFooterSize = new Size(pPageWidth, dblFooterHeight);
      szHeaderSize = new Size(pPageWidth, dblHeaderHeight);

      try {
        this.Source.DocumentPaginator.PageSize = szPageSize;
      }
      catch { }

    }

    // ----------------------------------------------------------------
    // This routine replaces specified document field keys with the 
    // actual values.  ie pagenumber, print date, etc.  Make as many 
    // as you see fit
    // ----------------------------------------------------------------
    private FlowDocument SetDocumentFields(FlowDocument sourceDoc, DocumentPage dp, int pageNum) {

      string sourceDocText = "";
      FlowDocument newDocument = new FlowDocument();

      // Get the FlowDocument as a big xml string.
      sourceDocText = XamlWriter.Save(sourceDoc);

      // a flowdocument is really nothing more than an xml document 
      // so lets load it into an xml dom
      XmlDocument xdoc = new XmlDocument();
      xdoc.LoadXml(sourceDocText);


      // now scan all the nodes in the xml document looking for text nodes.  
      // text nodes will contain text values which may be strings with key field 
      // identifiers.
      ProcessAndMaybeReplaceNodeTextWithFieldValues(xdoc, dp, pageNum);


      // get the xml from document as a string
      string s = xdoc.OuterXml;


      // create a string reader using the contents of the xml string
      StringReader stringReader = new StringReader(s);

      // create an xml reader from the xml string contained in the string reader
      XmlReader xmlReader = System.Xml.XmlReader.Create(stringReader);

      // load the xaml reader using the xml reader and create a flowDocument 
      newDocument = (FlowDocument)XamlReader.Load(xmlReader);

      // return the new flow document
      return newDocument;
    }

    // --------------------------------------------------------------------------------
    // ProcessXMLDocument is a recursive routine which traverses all nodes looking for 
    // text nodes and tries to replace any key text with the appropropriate values
    // --------------------------------------------------------------------------------
    private void ProcessAndMaybeReplaceNodeTextWithFieldValues(XmlNode xn, DocumentPage dp, int nPageNum) {

      PFSDocumentProperty p;

      foreach (XmlNode x in xn.ChildNodes) {
        // recurse.  
        ProcessAndMaybeReplaceNodeTextWithFieldValues(x, dp, nPageNum);

        // fall through.  Lets inspect the node.

        // if the node type is text, then we might have a key in there
        // so lets look for it.

        if (x.NodeType == XmlNodeType.Text) {
          // get value of node.
          string s = x.Value;

          // initialize counter.
          int idx = 0;

          // now we want to iterate through each document property and test if it exists in the 
          // nodes text value.  However, to speed things up, so we don't always iterate, we'll 
          // test for a prefix i.e. the existence of the first two characters of a property name. "${" 
          // if the prefix is in the string, then we'll keep replacing until no more properties are left 
          // to replace.  

          // Note, you have access to the page.  From that, you can get the visual so that you 
          // can do some drawing, etc. and append it to the Page Visual.  It's up to you to define 
          // the key field and processing.

          string strPageNumberFieldName = GetFullPropertyName(DOCUMENT_FIELD_NAME_PAGENUMBER);

          while ((s.IndexOf(strPropertyNamePrefix) >= 0) && (idx < DocumentProperties.Count)) {
            p = (PFSDocumentProperty)DocumentProperties[idx];

            if (p.Name == strPageNumberFieldName) {
              s = s.Replace(p.Name, (nPageNum + 1).ToString());
            } else {
              s = s.Replace(p.Name, p.Value);
            }

            idx += 1;
          }

          x.Value = s;
        }
      }

      return;
    }

    // ------------------------------------------------------------------------
    // Common Function To Initialize Flow Document Header and Footers
    // ------------------------------------------------------------------------
    private FlowDocument InitializeFlowDocument(FlowDocument fd, Table tbl, double dSectionHeight) {

      // if a header flow document was set, then use that
      if (fd != null) {
        // however, lets constrain the header flow document
        fd.PageHeight = dblHeaderHeight;
        fd.PageWidth = dblPageWidth - dblGutterLeft - dblMarginLeft - dblMarginRight - dblGutterRight;
        fd.MaxPageWidth = fd.PageWidth;
        fd.MaxPageHeight = fd.PageHeight;
      }

      // the header document was not defined, lets check to see if the header is defined as a table.
      else if (tbl != null) {
        fd = new FlowDocument();
        fd.ColumnWidth = dblPageWidth - dblGutterLeft - dblMarginLeft - dblMarginRight - dblGutterRight;
        fd.ColumnGap = 0;
        fd.PageHeight = dSectionHeight;
        fd.PageWidth = dblPageWidth - dblGutterLeft - dblMarginLeft - dblMarginRight - dblGutterRight;
        fd.PagePadding = new Thickness(0);
        fd.MaxPageWidth = fd.PageWidth;
        fd.MaxPageHeight = fd.PageHeight;
        fd.IsColumnWidthFlexible = false;
        fd.Blocks.Add(tbl);
      } else {
        fd = null;
      }

      return fd;
    }

    // ----------------------------------------------------------------------------
    // This function checks for header and footer document definition exceptions. 
    // i.e. if an odd page header is define, an even page header should be too.  
    // plus more.  Called By GetPage method, initialization code branch
    // -----------------------------------------------------------------------------
    private void CheckForExceptions() {


      // check if the defined margins and gutters don't exceed the page width
      if ((dblMarginLeft + dblMarginRight + dblGutterLeft + dblGutterRight) > dblPageWidth) {
        throw new System.InvalidOperationException(ERR_PAGE_WIDTH);
      }


      // check to see if the gutter, margins, header, and footer heights don't exceed the page height.
      if ((dblMarginTop + dblMarginBottom + dblFooterHeight + dblHeaderHeight + dblGutterTop + dblGutterBottom) > dblPageHeight) {
        throw new System.InvalidOperationException(ERR_PAGE_HEIGHT);
      }



      // if a header was defined, then lets see if everything is ok.
      if (boolProcessHeader) {
        // if the defeault header document is defined, then we are ok because that is the fall through catch all.  Otherwise, 
        // if it is null, we have to check if certain document sets defined.
        if (objHeaderFlowDocument == null) {

          if (objHeaderFlowDocumentOddPage != null) {
            if (objHeaderFlowDocumentEvenPage == null) {
              throw new System.InvalidOperationException(ERR_ODD_PAGE_HEADER_DEFINED_EVEN_PAGE_HEADER_NOT);
            }
          }

          if (objHeaderFlowDocumentEvenPage != null) {
            if (objHeaderFlowDocumentOddPage == null) {
              throw new System.InvalidOperationException(ERR_EVEN_PAGE_HEADER_DEFINED_ODD_PAGE_HEADER_NOT);
            }
          }

          if (objHeaderFlowDocumentFirstPage != null) {
            if (objHeaderFlowDocumentOddPage == null) {
              throw new System.InvalidOperationException(ERR_FIRST_PAGE_HEADER_DEFINED_ODD_PAGE_HEADER_NOT);
            }
          }

          if (objHeaderFlowDocumentFirstPage != null) {
            if (objHeaderFlowDocumentEvenPage == null) {
              throw new System.InvalidOperationException(ERR_FIRST_PAGE_HEADER_DEFINED_ODD_PAGE_HEADER_NOT);
            }
          }
        }

      }


      // If we are processing a footer, then lets check for invalid set definitions
      if (boolProcessFooter) {
        if (objFooterFlowDocument == null) {

          if (objFooterFlowDocumentOddPage != null) {
            if (objFooterFlowDocumentEvenPage == null) {
              throw new System.InvalidOperationException(ERR_ODD_PAGE_FOOTER_DEFINED_EVEN_PAGE_FOOTER_NOT);
            }
          }

          if (objFooterFlowDocumentEvenPage != null) {
            if (objFooterFlowDocumentOddPage == null) {
              throw new System.InvalidOperationException(ERR_EVEN_PAGE_FOOTER_DEFINED_ODD_PAGE_FOOTER_NOT);
            }
          }

          if (objFooterFlowDocumentFirstPage != null) {
            if (objFooterFlowDocumentOddPage == null) {
              throw new System.InvalidOperationException(ERR_FIRST_PAGE_FOOTER_DEFINED_ODD_PAGE_FOOTER_NOT);
            }
          }

          if (objFooterFlowDocumentFirstPage != null) {
            if (objFooterFlowDocumentEvenPage == null) {
              throw new System.InvalidOperationException(ERR_FIRST_PAGE_FOOTER_DEFINED_ODD_PAGE_FOOTER_NOT);
            }
          }
        }

      }



    }

    // --------------------------------------------------------------------------------
    // prints the doucment page.  This is the biggie.
    // --------------------------------------------------------------------------------
    public override DocumentPage GetPage(int pageNumber) {

      bool IsOddPage = false;
      bool IsEvenPage = false;

      // convoluted logic Page 1 is actually page 0.
      int intPageTypeResult = pageNumber % 2;
      if (intPageTypeResult == 0) {
        IsOddPage = true;
      } else {
        IsEvenPage = true;
      }

      // lets do a quick check to see if the content document was specified.  If not, 
      // then we have to throw an exception.
      if (objContentFlowDocument == null) {
        new System.InvalidOperationException(ERR_NO_CONTENT_FLOW_DOCUMENT_SPECIFIED);
      }


      // Define 3 Containers.  These will hold the header, footer, and 
      // content.  We'll merge all these together into 1 big Visual Container
      // just allocate these as news so the debugger doesn't throw flags.  
      // the reality is, these will be assigned.
      ContainerVisual HeaderCV = new ContainerVisual();
      ContainerVisual ContentCV = new ContainerVisual();
      ContainerVisual FooterCV = new ContainerVisual();
      ContainerVisual newPage = new ContainerVisual();

      FlowDocument processedHeaderDoc;
      FlowDocument processedFooterDoc;

      // what we will then do is check to see if the user set a flowdocument as a header.  If not, then 
      // check to see if a table was defined as a header.  If a table was defined as a header, we'll need to 
      // build the header flow document from the table.  If neither was defined, then there is no header, and 
      // we'll have to reset all the page sizes to accomodate for the non existence of the header.  

      // BEGIN IF NOT INITIALIZED (i.e. on the first page).
      if (!boolSystemIsInitialized) {

        // Fix the margin and gutter dimensions just in case they exceed 
        // page width and height definitions or are zero because of an improper 
        // specification
        FixMarginsAndGutters();


        // Initialize the header flow documents.
        objHeaderFlowDocument = InitializeFlowDocument(objHeaderFlowDocument, tblHeader, dblHeaderHeight);
        objHeaderFlowDocumentFirstPage = InitializeFlowDocument(objHeaderFlowDocumentFirstPage, tblHeaderFirstPage, dblHeaderHeight);
        objHeaderFlowDocumentOddPage = InitializeFlowDocument(objHeaderFlowDocumentOddPage, tblHeaderOddPage, dblHeaderHeight);
        objHeaderFlowDocumentEvenPage = InitializeFlowDocument(objHeaderFlowDocumentEvenPage, tblHeaderEvenPage, dblHeaderHeight);

        // if a header document is specified, lets set the process header flag to be true.
        if (objHeaderFlowDocument != null || objHeaderFlowDocumentFirstPage != null || objHeaderFlowDocumentOddPage != null || objHeaderFlowDocumentEvenPage != null) {

          boolProcessHeader = true;
        } else {
          dblHeaderHeight = 0;
        }

        // Initialize the Footer Flow Documents
        objFooterFlowDocument = InitializeFlowDocument(objFooterFlowDocument, tblFooter, dblFooterHeight);
        objFooterFlowDocumentFirstPage = InitializeFlowDocument(objFooterFlowDocumentFirstPage, tblFooterFirstPage, dblFooterHeight);
        objFooterFlowDocumentOddPage = InitializeFlowDocument(objFooterFlowDocumentOddPage, tblFooterOddPage, dblFooterHeight);
        objFooterFlowDocumentEvenPage = InitializeFlowDocument(objFooterFlowDocumentEvenPage, tblFooterEvenPage, dblFooterHeight);

        // if a footer document is specified, then lets set the process footer flag to be true
        if (objFooterFlowDocument != null || objFooterFlowDocumentFirstPage != null || objFooterFlowDocumentOddPage != null || objFooterFlowDocumentEvenPage != null) {
          boolProcessFooter = true;
        } else {
          dblFooterHeight = 0;
        }


        // now, lets check for exceptions, this is the error handler.  Note that 
        // if an exception is found, execution stops
        CheckForExceptions();


        // since we are in initialization, it always runs before the first
        // actual document page is printed.  Thus, we'll do a final resize on 
        // on the paginator so that the content gets printed appropriately.
        SetSizes();


        // here is a biggie.  We are going to override the page height and page 
        // widths specified in the source content flow document.  You may not like 
        // this so change it if you like
        objContentFlowDocument.PageHeight = szPageSize.Height;
        objContentFlowDocument.MaxPageHeight = objContentFlowDocument.PageHeight;
        objContentFlowDocument.PageWidth = szPageSize.Width;
        objContentFlowDocument.MaxPageWidth = objContentFlowDocument.PageWidth;


        // also, we'll compute the page count at this time
        this.Source.DocumentPaginator.ComputePageCount();


        // finally, lets do a quick initialization of the document properties
        InitializeDocumentProperties();

        // set the initialized flag to true so we don't 
        // have to keep repeating all this stuff.
        boolSystemIsInitialized = true;

      }  // END IF NOT INITIALIZED


      // start a new page container.  All header, footer, and content 
      // containers will append to this one.  Yes, I know it is already initialized.
      newPage = new ContainerVisual();

      // set capacity to 3 children.  Header, Footer, Content
      newPage.Children.Capacity = 3;


      // get the page that is generated from the default paginator. 
      // Note we do this first so that we can pass the page to the 
      // SetDocumentFields function.  That way, if you want to do some 
      // custom drawing, etc., you can get the page visual.
      DocumentPage page = this.Source.DocumentPaginator.GetPage(pageNumber);


      // Now let's do the header
      // if there is a header defined, it is in the objHeaderFlowDocument.  Let's use that
      if (boolProcessHeader) {

        // first, determine which header to print.
        if (objHeaderFlowDocumentFirstPage != null && pageNumber == 0) {
          processedHeaderDoc = SetDocumentFields(objHeaderFlowDocumentFirstPage, page, pageNumber);
        } else if (objHeaderFlowDocumentOddPage != null && IsOddPage) {
          processedHeaderDoc = SetDocumentFields(objHeaderFlowDocumentOddPage, page, pageNumber);
        } else if (objHeaderFlowDocumentEvenPage != null && IsEvenPage) {
          processedHeaderDoc = SetDocumentFields(objHeaderFlowDocumentEvenPage, page, pageNumber);
        } else {
          processedHeaderDoc = SetDocumentFields(objHeaderFlowDocument, page, pageNumber);
        }



        // now get the header
        IDocumentPaginatorSource headerSource = processedHeaderDoc;

        headerSource.DocumentPaginator.PageSize = new Size(this.Source.DocumentPaginator.PageSize.Width, dblHeaderHeight);
        DocumentPage HeaderDP = headerSource.DocumentPaginator.GetPage(0);


        // Change it to a ContainerVisual so we can append it to the new page
        HeaderCV = (ContainerVisual)HeaderDP.Visual;

        // now move the header visual to it's vertical position
        HeaderCV.Transform = new TranslateTransform(0, dblGutterTop + dblMarginTop);

        // now append it to the new page.
        newPage.Children.Add(HeaderCV);

      }




      // put the page visual into a container visual;            
      ContentCV = (ContainerVisual)page.Visual;

      // now transform it to the vertical position just below the header.  Note, if no header, then 
      // header height will be zero so we are ok.  Same logic for margins and gutter
      ContentCV.Transform = new TranslateTransform(0, dblGutterTop + dblMarginTop + dblHeaderHeight);

      // add it to the new page 
      newPage.Children.Add(ContentCV);


      // now lets do the footer.
      // if we have a footer, let's process it.
      if (boolProcessFooter) {

        // first, determine which footer to print.
        if (objFooterFlowDocumentFirstPage != null && pageNumber == 0) {
          processedFooterDoc = SetDocumentFields(objFooterFlowDocumentFirstPage, page, pageNumber);
        } else if (objFooterFlowDocumentOddPage != null && IsOddPage) {
          processedFooterDoc = SetDocumentFields(objFooterFlowDocumentOddPage, page, pageNumber);
        } else if (objFooterFlowDocumentEvenPage != null && IsEvenPage) {
          processedFooterDoc = SetDocumentFields(objFooterFlowDocumentEvenPage, page, pageNumber);
        } else {
          processedFooterDoc = SetDocumentFields(objFooterFlowDocument, page, pageNumber);
        }


        // now get the footer
        IDocumentPaginatorSource footerSource = processedFooterDoc;
        footerSource.DocumentPaginator.PageSize = new Size(this.Source.DocumentPaginator.PageSize.Width, dblFooterHeight);
        DocumentPage FooterDP = footerSource.DocumentPaginator.GetPage(0);

        // Change it to a ContainerVisual so we can append it to the new page
        FooterCV = (ContainerVisual)FooterDP.Visual;

        // now translate, move the footer to its vertical position just below the gutter, margin, header, and content
        FooterCV.Transform = new TranslateTransform(0, dblGutterTop + dblMarginTop + dblHeaderHeight + szPageSize.Height);

        // now append the footer visual to the new page
        newPage.Children.Add(FooterCV);

      }

      // now we create a new document page.  note that the size of this page is the full page height and width.
      // i.e. 8 1/2 x 11 for example (may be other dimensions
      DocumentPage dp = new DocumentPage(newPage, new Size(dblPageWidth, dblPageHeight),
                                         new Rect(0, 0, dblPageWidth, dblPageHeight),
                                         new Rect(0, 0, dblPageWidth, dblPageHeight));

      // now get the visual of the new document page
      ContainerVisual dpVisual = (ContainerVisual)dp.Visual;

      // perform a final translation on the new page moving it to the left 
      // i.e. left gutter plus left margin
      dpVisual.Transform = new TranslateTransform(dblGutterLeft + dblMarginLeft, 0);

      // return the new document page, we're outta here.
      return dp;
    }

    // -----------------------------------------------------------
    // This is a private class used to hold a document property 
    // name and value which are used to replace fields in a header 
    // or footer
    // -----------------------------------------------------------

    private class PFSDocumentProperty {
      private string strName = "";
      private string strValue = "";

      public string Name {
        get { return strName; }
        set { strName = value; }
      }

      public string Value {
        get { return strValue; }
        set { strValue = value; }
      }

    }  // end PFSDocumentProperty private class

  }
#endif

}
