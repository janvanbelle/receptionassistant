﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace ggReceptionHelper {

  public partial class HelperClass_Reception : IDisposable {

    #region export users to excel
    public string exportUsers(string fileName) {
      string result = "";
      DataTable userList = getUserList();
      FileInfo file = new FileInfo(fileName);
      ExcelPackage exportBook = null;
      try {
        exportBook = new ExcelPackage(file);
      }
      catch (Exception ex) {
        result = ex.Message;
        return result;
      }

      string curCompany = "none";
      ExcelWorksheet curSheet = null;
      int rowIndex = -1;
      foreach (DataRow user in userList.Select("badgeid > 0", "company, lastname, firstname")) {

        if (curCompany != user["company"].ToString().ToLower()) {
          curCompany = user["company"].ToString().ToLower();

          if (curCompany == "")
            curSheet = exportBook.Workbook.Worksheets.Add("no_name");
          else
            curSheet = exportBook.Workbook.Worksheets.Add(curCompany);

          rowIndex = 2;
          createHeaderRow(ref curSheet, rowIndex++);
        }

        createDataRow(ref curSheet, user, rowIndex++);

      }

      hideWorksheets(ref exportBook);

      exportBook.Workbook.Properties.Title = "export reception users";
      exportBook.Workbook.Properties.Company = "intrimmo";

      exportBook.Save();
      exportBook.Dispose();

      return result;
    }

    private void createHeaderRow(ref ExcelWorksheet sheet, int rowIndex) {
      sheet.Cells[rowIndex, 1].Value = "Badge ID";
      sheet.Cells[rowIndex, 2].Value = "Badge";
      sheet.Cells[rowIndex, 3].Value = "First name";
      sheet.Cells[rowIndex, 4].Value = "Last name";
      sheet.Cells[rowIndex, 5].Value = "Fixed phone";
      sheet.Cells[rowIndex, 6].Value = "Mobile";
      sheet.Cells[rowIndex, 7].Value = "Company";
      sheet.Cells[rowIndex, 8].Value = "SOP";
      sheet.Cells[rowIndex, 9].Value = "PhoneID";
 
      sheet.Column(1).Hidden = true;
      sheet.Column(2).AutoFit(9, 11);
      sheet.Column(2).Style.Numberformat.Format = "@"; 
      sheet.Column(3).AutoFit(9, 13);
      sheet.Column(4).AutoFit(12, 25);
      sheet.Column(5).AutoFit(12, 50);
      sheet.Column(5).Style.Numberformat.Format = "@";
      sheet.Column(6).AutoFit(12, 50);
      sheet.Column(6).Style.Numberformat.Format = "@";
      sheet.Column(7).Hidden = true;
      sheet.Column(8).Hidden = true;
      sheet.Column(9).Hidden = true;

      using (var range = sheet.Cells[rowIndex, 1, rowIndex, 10]) {
        range.Style.Font.Bold = false;
        range.Style.Fill.PatternType = ExcelFillStyle.Solid;
        range.Style.Fill.BackgroundColor.SetColor(Color.LightSteelBlue);
        range.Style.Font.Color.SetColor(Color.Black);
        range.Style.ShrinkToFit = false;
      }

     // sheet.Cells[1, 3].Value = "Users :";
     // sheet.Cells[1, 4].Value = "0";
    }

    private void createDataRow(ref ExcelWorksheet sheet, DataRow userData, int rowIndex) {
      int colIndex = 1;

      foreach (object value in userData.ItemArray) {
        sheet.Cells[rowIndex, colIndex++].Value = value;
      }

      sheet.Cells[1, 4].Value = rowIndex - 2;

    }

    private void hideWorksheets(ref ExcelPackage wb) {

      foreach (ExcelWorksheet sheet in wb.Workbook.Worksheets) {

        if ((sheet.Name == "presize") || (sheet.Name.Contains("intri"))) {
          sheet.Hidden = eWorkSheetHidden.Hidden;
          continue;
        }
        try {
          int lastRow = sheet.Dimension.End.Row;
          int lastCol = sheet.Dimension.End.Column - 1;
          var range = sheet.Cells[2, 1, lastRow, lastCol];
          
          range.AutoFilter = true;
        }
        catch (Exception ex) {
          string error = ex.Message;
        }
      }
    }
    #endregion

    #region import users from excel

    public string importUsers(string fileName) {
      string result = "";
      
      ExcelPackage importBook = null;
      try {
        FileInfo file = new FileInfo(fileName);
        importBook = new ExcelPackage(file);
      }
      catch (Exception ex) {
        result = ex.Message;
        return result;
      }
      

      foreach (ExcelWorksheet sheet in importBook.Workbook.Worksheets) {
        if (sheet.Hidden == eWorkSheetHidden.Hidden)
          continue;

        importExcelSheet(sheet);

      }

      importBook.Dispose();

      return result;
    }

    private string importExcelSheet(ExcelWorksheet ws) {
      string result = ""; 

      for (int rowNum = 3; rowNum <= ws.Dimension.End.Row; rowNum++) {
        int badgeid = Convert.ToInt32(ws.Cells[rowNum, 1].Value);
        string fixedNum = "";
        string mobileNum = "";

        try { fixedNum = ws.Cells[rowNum, 5].Value.ToString(); } catch { fixedNum = ""; }
        try { mobileNum = ws.Cells[rowNum, 6].Value.ToString(); } catch { mobileNum = ""; }

        updUser(badgeid, "extension", fixedNum);
        updUser(badgeid, "mobile", mobileNum);
      }
      return result;
    }



    #endregion

  }
}
