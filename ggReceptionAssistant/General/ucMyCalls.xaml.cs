﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
//using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using ggReceptionHelper;
using Hardcodet.Wpf.TaskbarNotification;

using FirstFloor.ModernUI.Windows.Controls;


namespace ggReceptionAssistant.General {

  /// <summary>
  /// Interaction logic for ucMyCalls.xaml
  /// </summary>
  public partial class ucMyCalls : UserControl {
    TaskbarIcon tbi;
    HelperClass_Reception helper;
    DispatcherTimer telTimer;
    MjpegDecoder camDecoder;
    bool isAdmin;

    HelperClass_Reception.phoneData curData, prevData;
    List<HelperClass_Reception.parkData> curParkData, prevParkData;
    //public activityKind callData;
    //string sopIP, mySOP, phoneID;

    public ucMyCalls() {
      InitializeComponent();

      helper = ((MainWindow)Application.Current.MainWindow).helper;
      isAdmin = ((MainWindow)Application.Current.MainWindow).isAdminVersion;

      telTimer = new DispatcherTimer();
      telTimer.Tick += telTimer_Tick;
      telTimer.Interval = new TimeSpan(0, 0, 1);

      camDecoder = null;

      if (isAdmin)
        gpbDebug.Visibility = Visibility.Visible;
      else
        gpbDebug.Visibility = Visibility.Hidden;

      string sopIP = Application.Current.Resources["sop_ip"].ToString(),
        phoneID = Application.Current.Resources["phone_id"].ToString();

      if (sopIP != "" && phoneID != "")
       telTimer.IsEnabled = true;

    }

    private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e) {
      //var result = e.NewValue;

      // het telefoonnummer kan 'on the fly' gewijzigd worden
      if ((bool)e.NewValue) {

        lblMyNumber.Content = helper.getSetting("RAPHONENUM");

      }
    }
    void telTimer_Tick(object sender, EventArgs e) {
      telTimer.IsEnabled = false;

      string sopIP = Application.Current.Resources["sop_ip"].ToString(),
        phoneID = Application.Current.Resources["phone_id"].ToString(),
        mySOP = Application.Current.Resources["sop_comp"].ToString();

      if (sopIP == "0.0.0.0") {
        telTimer.IsEnabled = true;
        lblNoConnection.Visibility = Visibility.Visible;
        return;
      } else {
        lblNoConnection.Visibility = Visibility.Hidden;
      }

      curData = helper.getPhoneState(sopIP, phoneID);
      curParkData = helper.getParkedChannels(sopIP);
      Application.Current.Resources["phoneState"] = curData;
      //Application.Current.Resources["parkedCalls"] = helper.getParkedChannels(sopIP);
      try {
        if (!CompareData("callData") ||
            !CompareData("parkData")) {
          tvwCalls.DataContext = processCallingData();

          if ((curData.state[0] == "Ringing") && 
              (curData.callerID[0] == "")) {
            // geen caller-info gevonden: switch de actieve SOP
            string passiveSOP = helper.getSOPip(mySOP, false);
            helper.sopSetActive(mySOP, passiveSOP);

            Application.Current.Resources["sop_ip"] = passiveSOP;
          }
        }
        
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      try {
        if (curData.state[0] != prevData.state[0]) {
          if (prevData.state != null)
            helper.addCallLogLine(ref curData);
          if (curData.state[0] == "Ringing") {
            tbi = (TaskbarIcon)FindResource("ReceptionIcon");
            tbi.Visibility = Visibility.Visible;
            string balloonTip = "call from : " + curData.callerID[0];
            string callerName = helper.getCaller(curData.callerID[0], mySOP);
            if (callerName != "")
              balloonTip += "\r\n" + callerName;
            tbi.ShowBalloonTip("info", balloonTip, BalloonIcon.Info);
          }
        }
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      prevData = curData;
      prevParkData = curParkData;

      telTimer.IsEnabled = true;
    }

    #region calling data processing
    private bool CompareData(string kind) {
      bool result = false;

      if (kind == "callData") {
        try {
          for (int i = 0; i < curData.state.Length; i++) {
            if ((curData.state[i] == prevData.state[i]) &&
                (curData.callerID[i] == prevData.callerID[i]) &&
                (curData.remoteChannel[i] == prevData.remoteChannel[i]))
              result = true;
            else
              result = false;
          }
        }
        catch (Exception ex) {
          string error = ex.Message;
          result = false;
        }

      }

      if (kind == "parkData") {
        try {
          if (curParkData.Count == prevParkData.Count) {
            result = true;
            foreach (HelperClass_Reception.parkData thisData in curParkData) {
              if (prevParkData.IndexOf(thisData) >= 0)
                result = true;
              else
                result = false;
            }
          }
        }
        catch (Exception ex) {
          string error = ex.Message;
          result = false;
        }
      }

      return result;
    }

    private void processTreeView() {
      
      foreach (TreeViewItem headerItem in tvwCalls.Items) {
        switch (headerItem.Header.ToString()) {
        case "No active call":
          headerItem.IsExpanded = false;
          break;

        default:
          headerItem.IsExpanded = true;
          break;
        }
      }

      tvwCalls.Items.Refresh();
    }

    private void addCallType (ref DataTable callTypes, string state) {
      DataRow callState = callTypes.NewRow();

      switch (state) {
      case "Idle":
        callState["id"] = "0";
        callState["kind"] = "No active call";
        break;

      case "Ringing":
        callState["id"] = "1";
        callState["kind"] = "Ringing";
        break;

      case "Up":
        callState["id"] = "2";
        callState["kind"] = "Call active";
        break;

      case "InternalConversation":
        callState["id"] = "2";
        callState["kind"] = "Call active";
        break;

      case "Parked":
        callState["id"] = "3";
        callState["kind"] = "Call on hold";
        break;

      default:
        callState["id"] = "4";
        callState["kind"] = state;
        break;

      }

      try {
        callTypes.Rows.Add(callState);
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

    }

    private DataSet processCallingData() {
      DataSet result = new DataSet();
      DataTable callData = new DataTable("callState");
      callData.PrimaryKey = new DataColumn[] { callData.Columns.Add("id", typeof(string)) };
      callData.Columns.Add("kind", typeof(string));

      DataTable callDetails = new DataTable("callDetails");
      callDetails.Columns.Add("parentid", typeof(string));
      callDetails.Columns.Add("num");
      callDetails.Columns.Add("name");
      callDetails.Columns.Add("channel");
      callDetails.Columns.Add("slot");

      int callIdx = 0;
      string mySOP = Application.Current.Resources["sop_comp"].ToString();
      string dbgState = "", dbgNum = "", dbgLocChan = "", dbgRemChan = "";

      // 1) process de actieve gesprekken 
      foreach (string caller in curData.callerID) {

        addCallType(ref callData, curData.state[callIdx]);

        DataRow callDetail = callDetails.NewRow();

        dbgState += curData.state[callIdx] + " - ";
        dbgNum += curData.callerID[callIdx] + " - ";
        dbgLocChan += curData.localChannel[callIdx] + " - ";
        dbgRemChan += curData.remoteChannel[callIdx] + " - ";

        switch (curData.state[callIdx]) {
        case "Idle":
          callDetail["parentid"] = "0";
          //callDetail["num"] = "";
          //callDetail["name"] = "";
          //callDetail["channel"] = "";
          //callDetail["slot"] = "";

          imgCamera.Visibility = Visibility.Hidden;

          if (camDecoder != null) {
            camDecoder.StopStream();

            camDecoder = null;
          }
          break;

        case "Ringing":
          callDetail["parentid"] = "1";
          callDetail["num"] = curData.callerID[callIdx];
          callDetail["name"] = helper.getCaller(curData.callerID[callIdx], mySOP);
          callDetail["channel"] = curData.remoteChannel[callIdx];
          callDetail["slot"] = "";

          if (((ModernWindow)Application.Current.MainWindow).WindowState == WindowState.Minimized)
            ((ModernWindow)Application.Current.MainWindow).WindowState = WindowState.Normal;

          ((ModernWindow)Application.Current.MainWindow).Topmost = true;
          ((ModernWindow)Application.Current.MainWindow).Topmost = false;

          ((ModernWindow)Application.Current.MainWindow).ContentSource = new Uri("/General/ucMyCalls.xaml", UriKind.Relative);

          DataTable thisphone = helper.getDoorphone(curData.callerID[0], mySOP);
          if (thisphone.Rows.Count > 0) {
            callDetail["name"] = showCamVideo(ref thisphone);
          }
          thisphone.Dispose();

          imgCamera.Visibility = Visibility.Visible;
          break;

        case "InternalConversation":
          callDetail["parentid"] = "2";
          callDetail["num"] = curData.callerID[callIdx];
          callDetail["name"] = helper.getCaller(curData.callerID[callIdx], mySOP);
          callDetail["channel"] = curData.remoteChannel[callIdx];
          callDetail["slot"] = "";

          imgCamera.Visibility = Visibility.Visible;
          break;

        case "Up":
          callDetail["parentid"] = "2";
          callDetail["num"] = curData.callerID[callIdx];
          callDetail["name"] = helper.getCaller(curData.callerID[callIdx], mySOP);
          callDetail["channel"] = curData.remoteChannel[callIdx];
          callDetail["slot"] = "";

          imgCamera.Visibility = Visibility.Visible;
          break;

        default:
          callDetail["parentid"] = "4";
          callDetail["num"] = curData.callerID[callIdx];
          callDetail["name"] = helper.getCaller(curData.callerID[callIdx], mySOP);
          callDetail["channel"] = curData.remoteChannel[callIdx];
          callDetail["slot"] = "";

          imgCamera.Visibility = Visibility.Visible;
          break;
        }

        if (curData.state[callIdx] != "Idle")
          callDetails.Rows.Add(callDetail);

        callIdx++;
      }
      if (isAdmin) {
        lblDbgState.Content = dbgState.Remove(dbgState.Length - 3);
        lblDbgCallerID.Content = dbgNum.Remove(dbgNum.Length - 3);
        lblDbgLocChannel.Content = dbgLocChan.Remove(dbgLocChan.Length - 3);
        lblDbgRemChannel.Content = dbgRemChan.Remove(dbgRemChan.Length - 3);
      }

      // 2) process de gesprekken 'on hold'
      foreach (HelperClass_Reception.parkData curData in curParkData) {
        addCallType(ref callData, "Parked");

        DataRow parkDetail = callDetails.NewRow();
        parkDetail["parentid"] = "3";
        parkDetail["num"] = curData.callerID;
        parkDetail["name"] = helper.getCaller(curData.callerID, mySOP);
        parkDetail["channel"] = curData.channelID;
        parkDetail["slot"] = curData.parkSlot.ToString();

        callDetails.Rows.Add(parkDetail);
      }

      // 3) dataset in orde zetten
      result.Tables.Add(callData);
      result.Tables.Add(callDetails);

      DataRelation dr = new DataRelation("rsParentChild", result.Tables["callState"].Columns["id"], result.Tables["callDetails"].Columns["parentid"]);
      dr.Nested = true;
      result.Relations.Add(dr);

      return result;
    }
    #endregion

    #region MJPEG display
    private string showCamVideo(ref DataTable camInfo) {
      string result = "";

      if (Convert.ToInt32(camInfo.Rows[0]["hascamera"]) == 1) {
        if (camDecoder != null) {
          camInfo.Dispose();
          return camInfo.Rows[0]["description"].ToString();
        }
        camDecoder = new MjpegDecoder();
        camDecoder.FrameReady += camDecoder_FrameReady;
        camDecoder.Error += camDecoder_Error;

        Uri camUri = new Uri("https://" + camInfo.Rows[0]["ip"].ToString() + "/api/camera/snapshot?width=320&height=240&source=internal&fps=4");

        camDecoder.ParseStream(camUri, "intrimmo", "GG126");
      } else {
        imgCamera.Source = (ImageSource)this.Resources["no-video"];
      }
      imgCamera.Visibility = Visibility.Visible;
      result = camInfo.Rows[0]["description"].ToString();

      return result;
    }

    private void camDecoder_Error(object sender, ggReceptionHelper.ErrorEventArgs e) {
      //MessageBox.Show(e.Message);
    }

    private void camDecoder_FrameReady(object sender, FrameReadyEventArgs e) {
      imgCamera.Source = e.BitmapImage;
    }
    #endregion

    #region call forwarding
    private void mbtnAction_Click(object sender, RoutedEventArgs e) {
      var test = e;
      ModernButton caller = ((ModernButton)sender);

      string sopIP = Application.Current.Resources["sop_ip"].ToString(),
             phoneID = Application.Current.Resources["phone_id"].ToString();

      try {
        switch (caller.CommandParameter.ToString()) {
        case "park":
          helper.callPark(sopIP, caller.Tag.ToString());
          break;

        case "transfer":
          helper.joinPark(sopIP, curData.remoteChannel[curData.remoteChannel.Length - 1], Convert.ToInt32(caller.Tag));
          break;

        case "takeback":
          helper.callRedirect(sopIP, caller.Tag.ToString(), "0" + lblMyNumber.Content.ToString().Replace(" ","").Replace("+", "00"));
          break;
        }
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
      
    }

    private void ModernButton_MouseEnter(object sender, MouseEventArgs e) {
      telTimer.IsEnabled = false;
    }

    private void ModernButton_MouseLeave(object sender, MouseEventArgs e) {
      telTimer.IsEnabled = true;
    }
    #endregion



    

  }
}
