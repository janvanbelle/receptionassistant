﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using ggReceptionHelper;

namespace ggReceptionAssistant.General {
  /// <summary>
  /// Interaction logic for ucCallTesting.xaml
  /// </summary>
  public partial class ucCallTesting : UserControl {
    //HLP_SQLite sqlHelper;
    //HLP_Escaux telHelper;
    HelperClass_Reception helper;
    MjpegDecoder _mjpeg;

    public ucCallTesting() {
      InitializeComponent();

      helper = ((MainWindow)(Application.Current.MainWindow)).helper;

      _mjpeg = null;

    }

    private void UserControl_Loaded(object sender, RoutedEventArgs e) {
      lblRaPhoneID.Content = Application.Current.Resources["phone_id"].ToString();
    }

    #region call controll
    private void btnInitCall_Click(object sender, RoutedEventArgs e) {
      string sopIP = helper.getSetting("RAPHONESOPIP");

      helper.callInitiate(sopIP, lblRaPhoneID.Content.ToString(), tbInitCall.Text);
    }

    private void btnHangupCall_Click(object sender, RoutedEventArgs e) {
      string sopIP = helper.getSetting("RAPHONESOPIP");

      //(sopIP, lblRaPhoneID.Content.ToString(), tbInitCall.Text);
    }

    #endregion

    #region call parking
    private void btnRedirectCall_Click(object sender, RoutedEventArgs e) {
      string sopIP = helper.getSetting("RAPHONESOPIP");
      string phoneID = helper.getSetting("RAPHONEID");

      HelperClass_Reception.phoneData curData = helper.getPhoneState(sopIP, phoneID);
      if (btnJoinPark.Tag.ToString() != "") {
        helper.callRedirect(sopIP, btnJoinPark.Tag.ToString(), "029");
      } else {
        helper.callRedirect(sopIP, curData.remoteChannel[0], tbRedirectCall.Text);
      }
      btnJoinPark.Tag = "";
    }

    private void btnParkCall_Click(object sender, RoutedEventArgs e) {
      string sopIP = helper.getSetting("RAPHONESOPIP");
      string phoneID = helper.getSetting("RAPHONEID");

      HelperClass_Reception.phoneData curData = helper.getPhoneState(sopIP, phoneID);

      helper.callPark(sopIP, curData.remoteChannel[0]);
      //helper.callRedirect(sopIP, curData.remoteChannel, "89990005");
      btnJoinPark.Tag = curData.remoteChannel[0];

    }

    private void btnJoinPark_Click(object sender, RoutedEventArgs e) {
      string sopIP = helper.getSetting("RAPHONESOPIP");
      string phoneID = helper.getSetting("RAPHONEID");

      HelperClass_Reception.phoneData curData = helper.getPhoneState(sopIP, phoneID);

      //telHelper.callPark(sopIP, curData.remoteChannel);
      helper.callRedirect(sopIP, btnJoinPark.Tag.ToString(), "89990003");
    }
    #endregion

    #region doorphone camera
    private void btnShowCamera_Click(object sender, RoutedEventArgs e) {
      string cameraIP = tbCameraIP.Text;
      string APICommand = tbCameraAPI.Text;

      MemoryStream camImage = helper.camFetchSingle(cameraIP, APICommand);  
      try {
        camImage.Position = 0;

        var imgSource = new BitmapImage();
        imgSource.BeginInit();
        imgSource.StreamSource = camImage;
        imgSource.EndInit();

        imgCamera.Source = imgSource;
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
    }

    private void btnShowMJPeg_Click(object sender, RoutedEventArgs e) {
      string cameraIP = tbCameraIP.Text;
      string APICommand = tbCameraAPI.Text;

      if (_mjpeg == null) {
        _mjpeg = new MjpegDecoder();
        _mjpeg.FrameReady += _mjpeg_FrameReady;
        _mjpeg.Error += _mjpeg_Error;

        Uri camUri = new Uri("https://" + cameraIP + APICommand + "&fps=3");

        _mjpeg.ParseStream(camUri, "intrimmo", "GG126");
      } else {
        _mjpeg.StopStream();
        _mjpeg = null;
      }
    }

    private void _mjpeg_Error(object sender, ggReceptionHelper.ErrorEventArgs e) {
      MessageBox.Show(e.Message);
    }

    private void _mjpeg_FrameReady(object sender, FrameReadyEventArgs e) {
      imgCamera.Source = e.BitmapImage;
    }


    #endregion

    private void btnGetParkedChannels_Click(object sender, RoutedEventArgs e) {
      string sopIP = helper.getSetting("RAPHONESOPIP");
      string phoneID = helper.getSetting("RAPHONEID");

      helper.getParkedChannels(sopIP);
    }
  }
}
