﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using ggReceptionHelper;

namespace ggReceptionAssistant.Settings {
  /// <summary>
  /// Interaction logic for ucSettingsEntrance.xaml
  /// </summary>
  public partial class ucSettingsEntrance : UserControl {
    HelperClass_Reception helper;
    bool isAdmin;

    public ucSettingsEntrance() {
      InitializeComponent();

      helper = ((MainWindow)(Application.Current.MainWindow)).helper;
      isAdmin = ((MainWindow)(Application.Current.MainWindow)).isAdminVersion;
    }

    private void UserControl_Loaded(object sender, RoutedEventArgs e) {
      dgEntrances.DataContext = helper.getEntranceList();

      if (!isAdmin) {
        btnChangeEntrance.IsEnabled = false;
        chkExtern.IsEnabled = false;
      }
    }

    private void dgEntrances_SelectionChanged(object sender, SelectionChangedEventArgs e) {
      if (e.AddedItems.Count == 0)
        return;

      if (!isAdmin)
        return;

      try {
        DataRowView selection = (e.AddedItems[0] as DataRowView);

        lblEntranceName.Content = selection["name"].ToString();
        lblEntranceName.Tag = selection["controllerid"];
        tbEntranceZone.Text = selection["zone"].ToString();
        chkExtern.IsChecked = Convert.ToBoolean(selection["toextern"]);

        btnChangeEntrance.IsEnabled = true;
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
    }

    private void btnChangeEntrance_Click(object sender, RoutedEventArgs e) {
      int controller = Convert.ToInt32(lblEntranceName.Tag);
      helper.updEntrance(controller, "zone", tbEntranceZone.Text);
      helper.updEntrance(controller, "toextern", ((bool)chkExtern.IsChecked ? "1" : "0"));

      chkExtern.IsChecked = false;
      tbEntranceZone.Text = "";
      lblEntranceName.Content = "";
      btnChangeEntrance.IsEnabled = false;

      dgEntrances.DataContext = helper.getEntranceList();
    }
  }
}
