﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using ggReceptionHelper;
using FirstFloor.ModernUI.Windows.Controls;
using Microsoft.Win32;

namespace ggReceptionAssistant.Settings {
  /// <summary>
  /// Interaction logic for ucSettingsPhone.xaml
  /// </summary>
  public partial class ucSettingsPhone : UserControl {
    HelperClass_Reception helper;
    bool isAdmin;

    public ucSettingsPhone() {
      InitializeComponent();

      helper = ((MainWindow)(Application.Current.MainWindow)).helper;
      isAdmin = ((MainWindow)(Application.Current.MainWindow)).isAdminVersion;

      if (isAdmin) {
        cbbSopList.IsEnabled = true;
        cbbPhoneList.IsEnabled = true;
        btnSave.IsEnabled = true;
        tbPhoneNumber.IsEnabled = true;
      } else {
        cbbSopList.IsEnabled = false;
        cbbPhoneList.IsEnabled = false;
        btnSave.IsEnabled = false;
        tbPhoneNumber.IsEnabled = false;
      }
      DataTable sopList = helper.getSOPlist();
      sopList.DefaultView.RowFilter = "isprimary = 1";
      cbbSopList.DataContext = sopList.DefaultView.ToTable();

      cbbSopList.Text = helper.getSetting("RAPHONESOP");
      try {
        cbbPhoneList.DataContext = helper.getPhoneList(cbbSopList.SelectedItem.ToString());
      }
      catch { }
      cbbPhoneList.Text = helper.getSetting("RAPHONENAME");
      if (cbbPhoneList.SelectedIndex == -1) {
        cbbPhoneList.IsEnabled = false;
      }
      tbPhoneNumber.Text = helper.getSetting("RAPHONENUM");
    }

    private void cbbSopList_SelectionChanged(object sender, SelectionChangedEventArgs e) {
      DataTable phoneList;
      string selectedSOP = "";

      if (e.AddedItems.Count == 0)
        return;

      DataRowView selection = e.AddedItems[0] as DataRowView;
      selectedSOP = selection.Row["company"].ToString();
      try {
        phoneList = helper.getPhoneList(selectedSOP);
      }
      catch {
        phoneList = new DataTable();
      }

      if (phoneList.Rows.Count == 0) {
        ModernDialog.ShowMessage("Please synchronise the SOP users", "error", MessageBoxButton.OK);

        //string sopIP = helper.getSOPip(selectedSOP, true); // !!!!!

        //DataTable directory = helper.getUserDirectory(sopIP);

        //helper.startTransaction();
        //helper.delAllUsers(selectedSOP);
        //foreach (DataRow user in directory.Rows) {
        //  helper.addUser(selectedSOP, user["firstName"].ToString(), user["lastName"].ToString(), user["extension"].ToString());
        //  helper.updUser(selectedSOP, user["extension"].ToString(), "phoneid", user["phoneid"].ToString());
        //  helper.updUser(selectedSOP, user["extension"].ToString(), "email", user["email"].ToString());
        //  helper.updUser(selectedSOP, user["extension"].ToString(), "department", user["department"].ToString());
        //  helper.updUser(selectedSOP, user["extension"].ToString(), "mobile", user["mobile"].ToString());
        //  helper.updUser(selectedSOP, user["extension"].ToString(), "company", user["company"].ToString());
        //}
        //helper.commitTransaction();
        return;
      }

      cbbPhoneList.DataContext = helper.getPhoneList(selectedSOP);
      cbbPhoneList.IsEnabled = true;

    }

    private void btnSave_Click(object sender, RoutedEventArgs e) {
      if (cbbSopList.SelectedIndex != -1) {
        if (helper.addSetting("RAPHONESOP", cbbSopList.Text.ToString()) != "") {
          helper.delSetting("RAPHONESOP");
          helper.addSetting("RAPHONESOP", cbbSopList.Text.ToString());
          Application.Current.Resources["sop_comp"] = cbbSopList.Text.ToString();
        }
        if (helper.addSetting("RAPHONESOPIP", cbbSopList.SelectedValue.ToString()) != "") {
          helper.delSetting("RAPHONESOPIP");
          helper.addSetting("RAPHONESOPIP", cbbSopList.SelectedValue.ToString());
          Application.Current.Resources["sop_ip"] = cbbSopList.SelectedValue.ToString();
        }
      } else {
        ModernDialog.ShowMessage("SOP not changed", "remark", MessageBoxButton.OK);
      }

      if (cbbPhoneList.SelectedIndex != -1) {
        if (helper.addSetting("RAPHONENAME", cbbPhoneList.Text.ToString()) != "") {
          helper.delSetting("RAPHONENAME");
          helper.addSetting("RAPHONENAME", cbbPhoneList.Text.ToString());
        }
        if (helper.addSetting("RAPHONEID", cbbPhoneList.SelectedValue.ToString()) != "") {
          helper.delSetting("RAPHONEID");
          helper.addSetting("RAPHONEID", cbbPhoneList.SelectedValue.ToString());
          Application.Current.Resources["phone_id"] = cbbPhoneList.SelectedValue.ToString();
        }

      } else {
        ModernDialog.ShowMessage("PHONE not changed", "remark", MessageBoxButton.OK);
      }

      if (helper.addSetting("RAPHONENUM", tbPhoneNumber.Text) != "") {
        helper.delSetting("RAPHONENUM");
        helper.addSetting("RAPHONENUM", tbPhoneNumber.Text);
      }

    }

    #region export users
    private void btnExportUsers_Click(object sender, RoutedEventArgs e) {
      SaveFileDialog save = new SaveFileDialog();
      save.Filter = "Excel 2007+(*.xlsx)|*.xlsx|Excel 2003(*.xls)|*.xls|All(*.*)|*";
      if (save.ShowDialog() == false)
        return;

      btnExportUsers.IsEnabled = false;
      btnImportUsers.IsEnabled = false;
      mprProgress.IsActive = true;

      BackgroundWorker export = new BackgroundWorker();
      export.WorkerReportsProgress = false;
      export.RunWorkerCompleted += export_RunWorkerCompleted;
      export.DoWork += export_DoWork;
      export.RunWorkerAsync(save.FileName);

    }

    private void export_DoWork(object sender, DoWorkEventArgs e) {
      e.Result = helper.exportUsers(e.Argument.ToString());
    }

    private void export_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
      btnExportUsers.IsEnabled = true;
      btnImportUsers.IsEnabled = true;
      mprProgress.IsActive = false;

      if (e.Result.ToString() != "")
        ModernDialog.ShowMessage(e.Result.ToString(), "error", MessageBoxButton.OK);
    }
    #endregion

    #region import users
    private void btnImportUsers_Click(object sender, RoutedEventArgs e) {
      OpenFileDialog open = new OpenFileDialog();
      open.Filter = "Excel 2007+(*.xlsx)|*.xlsx|Excel 2003(*.xls)|*.xls|All(*.*)|*";
      if (open.ShowDialog() == false)
        return;

      btnExportUsers.IsEnabled = false;
      btnImportUsers.IsEnabled = false;
      mprProgress.IsActive = true;

      BackgroundWorker import = new BackgroundWorker();
      import.WorkerReportsProgress = false;
      import.RunWorkerCompleted += import_RunWorkerCompleted;
      import.DoWork += import_DoWork;
      import.RunWorkerAsync(open.FileName);
    }

    private void import_DoWork(object sender, DoWorkEventArgs e) {
      e.Result = helper.importUsers(e.Argument.ToString());
    }

    private void import_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
      btnExportUsers.IsEnabled = true;
      btnImportUsers.IsEnabled = true;
      mprProgress.IsActive = false;

      if (e.Result.ToString() != "")
        ModernDialog.ShowMessage(e.Result.ToString(), "error", MessageBoxButton.OK);
    }
    #endregion

    private void btnPrintUsers_Click(object sender, RoutedEventArgs e) {
      OpenFileDialog open = new OpenFileDialog();
      open.Filter = "Excel 2007+(*.xlsx)|*.xlsx|Excel 2003(*.xls)|*.xls|All(*.*)|*";
      if (open.ShowDialog() == false)
        return;


    }

    private void btnLogo_Click(object sender, RoutedEventArgs e) {
      OpenFileDialog open = new OpenFileDialog();
      open.Filter = "All graphics|*.png;*.jpg;*.gif;*.bmp|Portable Network Graphics (.png)|*.png|JPEG (.jpg)|*.jpg|GIF (.gif)|*.gif|Bitmap (.bmp)|*.bmp";
      if (open.ShowDialog() == false)
        return;

      var uriSource = new Uri(open.FileName, UriKind.Absolute);
      imgLogo.Source = new BitmapImage(uriSource);

      if (helper.addSetting("LOGOPATH", open.FileName) != "") {
        helper.delSetting("LOGOPATH");
        helper.addSetting("LOGOPATH", open.FileName);
      }
    }
  }
}
