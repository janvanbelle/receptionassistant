﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using ggReceptionHelper;

namespace ggReceptionAssistant.General {
  /// <summary>
  /// Interaction logic for ucUserSecurity.xaml
  /// </summary>
  public partial class ucUserSecurity : UserControl {
    HelperClass_Reception helper;
   // Thread printThread;
   // DispatcherTimer printTimer;

    struct fetchInfo {
      public int fetchType;
      public DateTime filterTime;
      public int filterCarrier;
    }

    public ucUserSecurity() {
      InitializeComponent();

      helper = ((MainWindow)(Application.Current.MainWindow)).helper;

      dpFilterDate.SelectedDate = DateTime.Today;
      tbFilterTime.Text = DateTime.Now.Hour.ToString("00") + ":" + DateTime.Now.Minute.ToString("00");

    }

    private void btnPrintPresence_Click(object sender, RoutedEventArgs e) {

      Thread printThread = new Thread(() => {
        helper.printUserPresence(DateTime.Now, true);
      });

      printThread.SetApartmentState(ApartmentState.STA);
      printThread.Start();

    }

    private void cbbReportType_SelectionChanged(object sender, SelectionChangedEventArgs e) {
      try {
        dgSecuData.DataContext = new DataTable();

        switch (((ComboBoxItem)e.AddedItems[0]).Tag.ToString()) {
        case "0":
          lblReportUser.Visibility = Visibility.Hidden;
          cbbReportUser.Visibility = Visibility.Hidden;
          btnSearch.IsEnabled = false;
          btnPrint.IsEnabled = false;

          dgSecuData.Columns[0].Visibility = Visibility.Hidden;
          dgSecuData.Columns[5].Visibility = Visibility.Hidden;
          break;

        case "1":
          lblReportUser.Visibility = Visibility.Hidden;
          cbbReportUser.Visibility = Visibility.Hidden;
          btnSearch.IsEnabled = true;
          btnPrint.IsEnabled = false;

          dgSecuData.Columns[0].Visibility = Visibility.Hidden;
          dgSecuData.Columns[4].Visibility = Visibility.Visible;
          dgSecuData.Columns[5].Visibility = Visibility.Hidden;
          break;

        case "2":
          DataTable users = helper.getUsers("no_user");
          DataRow newUser = users.NewRow();
          newUser["badgeid"] = -1;
          newUser["Name"] = "no specific user";
          users.Rows.InsertAt(newUser, 0);

          cbbReportUser.DataContext = users;

          lblReportUser.Visibility = Visibility.Visible;
          cbbReportUser.Visibility = Visibility.Visible;

          btnSearch.IsEnabled = false;
          btnPrint.IsEnabled = false;

          dgSecuData.Columns[0].Visibility = Visibility.Visible;
          dgSecuData.Columns[4].Visibility = Visibility.Hidden;
          dgSecuData.Columns[5].Visibility = Visibility.Visible;
          cbbReportUser.IsDropDownOpen = false;
          cbbReportUser.Tag = -1;
          break;
        }

      }
      catch (Exception ex) {
        string error = ex.Message;
      }
    }

    

    private void btnSearch_Click(object sender, RoutedEventArgs e) {
      int tagValue = -1;
      string[] timeValues = tbFilterTime.Text.Split(':');
      DateTime filterTime = (DateTime)dpFilterDate.SelectedDate;

      try {
        tagValue = Convert.ToInt32(((ComboBoxItem)cbbReportType.SelectedItem).Tag);
      }
      catch (Exception ex) {
        string error = ex.Message;
        cbbReportType.Focus();
        return;
      }

      try {
        filterTime = filterTime.AddHours(Convert.ToInt16(timeValues[0]));
        filterTime = filterTime.AddMinutes(Convert.ToInt16(timeValues[1]));
      }
      catch (Exception ex) {
        string error = ex.Message;
        tbFilterTime.Focus();
        return;
      }

      btnSearch.IsEnabled = false;
      cbbReportType.IsEnabled = false;
      btnPrint.IsEnabled = false;
      mprProgress.IsActive = true;

      fetchInfo tmpData = new fetchInfo();
      tmpData.fetchType = tagValue;
      tmpData.filterTime = filterTime;
      tmpData.filterCarrier = Convert.ToInt32(cbbReportUser.Tag);

      BackgroundWorker bgSearcher = new BackgroundWorker();
      bgSearcher.WorkerReportsProgress = false;
      bgSearcher.RunWorkerCompleted += BgSearcher_RunWorkerCompleted;
      bgSearcher.DoWork += BgSearcher_DoWork;
      bgSearcher.RunWorkerAsync(tmpData);

    }

    private void cbbReportUser_SelectionChanged(object sender, SelectionChangedEventArgs e) {
      try {
        ComboBoxItem firstRow = (ComboBoxItem)e.AddedItems[0];
        cbbReportUser.Tag = -1;
        btnSearch.IsEnabled = true;
        cbbReportUser.Text = firstRow.Content.ToString();
        return;
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      try {
        DataRowView userData = (DataRowView)e.AddedItems[0];

        cbbReportUser.Tag = userData["badgeid"];
        btnSearch.IsEnabled = true;
      }
      catch (Exception ex) {
        string error = ex.Message;

        cbbReportUser.Tag = -1;
      }

    }

    private void cbbReportUser_TextChanged(object sender, RoutedEventArgs e) {
      
      try {
        if (cbbReportUser.Text.Length < 3)
          return;

        if (cbbReportUser.Text == "no specific user")
          return;

        if (Convert.ToInt32(cbbReportUser.Tag) == Convert.ToInt32(cbbReportUser.SelectedValue))
          return;

        //if (cbbReportUser.Items.Count > 0) {
        //  cbbReportUser.Items.Clear();
        //  return;
        //}

        DataTable users = helper.getUsers(cbbReportUser.Text);
        DataRow newUser = users.NewRow();
        newUser["badgeid"] = -1;
        newUser["Name"] = "no specific user";
        users.Rows.InsertAt(newUser, 0);

        cbbReportUser.DataContext = users;
        cbbReportUser.Tag = -1;
        cbbReportUser.IsDropDownOpen = true;
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
    }

    #region background print
    //private async void BgPrinter_DoWork () { // (object sender, DoWorkEventArgs e) { 
    //  helper.printUserPresence(DateTime.Now);
    //}

    //private void BgPrinter_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
    //  btnPrintPresence.IsEnabled = true;
    //  mprProgress.IsActive = false;
    //}
    #endregion

    #region background fetch data
    private void BgSearcher_DoWork(object sender, DoWorkEventArgs e) {

      fetchInfo tmpData = (fetchInfo)e.Argument;

      switch (tmpData.fetchType) {
      case 1:
        try {
          e.Result = helper.getUserPresence(tmpData.filterTime).Tables["presence"];
        }
        catch (Exception ex) {
          string error = ex.Message;
        }
        break;

      case 2:
        try {
          helper.openSecuConnection();
          if (tmpData.filterCarrier == -1) {
            e.Result = helper.getSecuEvents(-1, tmpData.filterTime.AddHours(-1), tmpData.filterTime);
          } else {
            e.Result = helper.getSecuEvents(tmpData.filterCarrier, tmpData.filterTime.Date, tmpData.filterTime);
          }
        }
        catch (Exception ex) {
          string error = ex.Message;
        }
        finally {
          helper.closeSecuConnection();
        }
        break;
      }
    }

    private void BgSearcher_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
      btnSearch.IsEnabled = true;
      cbbReportType.IsEnabled = true;
      btnPrint.IsEnabled = true;
      mprProgress.IsActive = false;

      dgSecuData.DataContext = e.Result;
    }

    #endregion

    private void btnPrint_Click(object sender, RoutedEventArgs e) {
      int tagValue = -1;
      string[] timeValues = tbFilterTime.Text.Split(':');
      DateTime filterTime = (DateTime)dpFilterDate.SelectedDate;
      int carrierID = Convert.ToInt32(cbbReportUser.Tag);

      try {
        tagValue = Convert.ToInt32(((ComboBoxItem)cbbReportType.SelectedItem).Tag);
      }
      catch (Exception ex) {
        string error = ex.Message;
        cbbReportType.Focus();
        return;
      }

      try {
        filterTime = filterTime.AddHours(Convert.ToInt16(timeValues[0]));
        filterTime = filterTime.AddMinutes(Convert.ToInt16(timeValues[1]));
      }
      catch (Exception ex) {
        string error = ex.Message;
        tbFilterTime.Focus();
        return;
      }

      Thread printThread = new Thread(() => {
        if (tagValue == 1) {
          helper.printUserPresence(filterTime, false);
        } else {
          
          helper.printUserEvents(carrierID, filterTime);
        }
      });

      printThread.SetApartmentState(ApartmentState.STA);
      printThread.Start();
    }
  }
}
