﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using ggReceptionHelper;
using FirstFloor.ModernUI.Windows.Controls;

namespace ggReceptionAssistant.Settings {
  /// <summary>
  /// Interaction logic for ucSettingsSecuritas.xaml
  /// </summary>
  public partial class ucSettingsSecuritas : UserControl {
    HelperClass_Reception helper;
    bool isAdmin;

    public ucSettingsSecuritas() {
      InitializeComponent();

      helper = ((MainWindow)(Application.Current.MainWindow)).helper;
      isAdmin = ((MainWindow)(Application.Current.MainWindow)).isAdminVersion;

      tbAeosServer.Text = helper.getSetting("AEOSSRV");
      tbAeosDatabase.Text = helper.getSetting("AEOSDB");
      tbAeosUser.Text = helper.getSetting("AEOSUSR");
      pwbAeosPassword.Password = helper.getSetting("AEOSPWD");

      if (!isAdmin) {
        tbAeosServer.IsEnabled = false;
        tbAeosDatabase.IsEnabled = false;
        tbAeosUser.IsEnabled = false;
        pwbAeosPassword.IsEnabled = false;

        btnSave.IsEnabled = false;
      }
    }

    private void btnSave_Click(object sender, RoutedEventArgs e) {

      helper.delSetting("AEOSSRV");
      helper.delSetting("AEOSDB");
      helper.delSetting("AEOSUSR");
      helper.delSetting("AEOSPWD");
      helper.delSetting("AEOS_CONNECT");

      helper.addSetting("AEOSSRV", tbAeosServer.Text);
      helper.addSetting("AEOSDB", tbAeosDatabase.Text);
      helper.addSetting("AEOSUSR", tbAeosUser.Text);
      helper.addSetting("AEOSPWD", pwbAeosPassword.Password);

      string AEOSsvrConnection =
        "Initial Catalog=" + tbAeosDatabase.Text + ";" +
        "Data Source=" + tbAeosServer.Text + ";" +
        "Persist Security Info=True;" +
        "User ID=" + tbAeosUser.Text + ";" +
        "Password=" + pwbAeosPassword.Password + ";";
      try {
        if (helper.TestSecuConnection(AEOSsvrConnection)) {
          helper.addSetting("AEOS_CONNECT", AEOSsvrConnection);
        }
      }
      catch (Exception ex) {
        ModernDialog.ShowMessage("Error connecting to AEOS database:\r\n" + ex.Message, "error", MessageBoxButton.OK);
      }
      
    }

    private void btnSynchronize_Click(object sender, RoutedEventArgs e) {
      mprProgress.IsActive = true;
      btnSave.IsEnabled = false;
      btnSynchronize.IsEnabled = false;

      BackgroundWorker synch = new BackgroundWorker();
      synch.WorkerReportsProgress = false;
      synch.RunWorkerCompleted += Synch_RunWorkerCompleted;
      synch.DoWork += Synch_DoWork;
      synch.RunWorkerAsync();
    }

    private void Synch_DoWork(object sender, DoWorkEventArgs e) {
      helper.openSecuConnection();
      helper.syncSecuUsers();

      /*
      DataTable secuUsers = helper.getSecuUsers();
      DataTable localUsers = helper.getUserList();

      //Thread.Sleep(5000);
      helper.startTransaction();
      foreach (DataRow secuUser in secuUsers.Rows) {
        DataRow[] localUser = new DataRow[0];
        try {
          string selectFilter = "badgeid=" + secuUser["badgeid"].ToString();
          localUser = localUsers.Select(selectFilter);
        }
        catch { }

        if (localUser.Length == 0) {
          helper.addUser(Convert.ToInt32(secuUser["badgeid"]), secuUser["badge"].ToString(), 
            secuUser["firstname"].ToString(), secuUser["lastname"].ToString(), secuUser["company"].ToString());
        } else {
          helper.updUser(Convert.ToInt32(secuUser["badgeid"]), "firstname", secuUser["firstname"].ToString());
          helper.updUser(Convert.ToInt32(secuUser["badgeid"]), "lastname", secuUser["lastname"].ToString());
          helper.updUser(Convert.ToInt32(secuUser["badgeid"]), "company", secuUser["company"].ToString());
        }
      }
      helper.commitTransaction();

      foreach (DataRow localUser in localUsers.Rows) {
        DataRow[] secuUser = new DataRow[0];
        try {
          string selectFilter = "badgeid=" + localUser["badgeid"].ToString();
          secuUser = secuUsers.Select(selectFilter);
        }
        catch { }

        if (secuUser.Length == 0) {
          helper.delUser(Convert.ToInt32(localUser["badgeid"]));
        }
      }

      secuUsers.Dispose();
      localUsers.Dispose();

      DataTable secuEntrances = helper.getSecuEntrances();
     // DataTable localEntrances = helper.gete

      foreach (DataRow access in secuEntrances.Rows) {
        helper.addEntrance(Convert.ToInt32(access["entranceid"]), access["name"].ToString(), Convert.ToInt32(access["unlockscheduleid"]), access["unlockschedule"].ToString());
      }

      secuEntrances.Dispose();
      */
    }

    private void Synch_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
      helper.closeSecuConnection();
      mprProgress.IsActive = false;
      btnSave.IsEnabled = true;
      btnSynchronize.IsEnabled = true;
      //throw new NotImplementedException();
    }
  }
}
