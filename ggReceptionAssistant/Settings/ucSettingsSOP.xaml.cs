﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using ggReceptionHelper;
using FirstFloor.ModernUI.Windows.Controls;

namespace ggReceptionAssistant.Settings {
  /// <summary>
  /// Interaction logic for ucSettingsSOP.xaml
  /// </summary>
  public partial class ucSettingsSOP : UserControl {
    //HLP_SQLite sqlHelper;
    HelperClass_Reception helper;
    bool isAdmin;

    public ucSettingsSOP() {
      InitializeComponent();

      //sqlHelper = ((MainWindow)(Application.Current.MainWindow)).sqlHelper;
      helper = ((MainWindow)(Application.Current.MainWindow)).helper;
      isAdmin = ((MainWindow)(Application.Current.MainWindow)).isAdminVersion;

      if (isAdmin) {
        btnAddSOP.IsEnabled= true;
        btnDelSOP.IsEnabled = true;
      } else {
        btnAddSOP.IsEnabled = false;
        btnDelSOP.IsEnabled = false;
      }
      dgSopList.DataContext = helper.getSOPlist();

    }

    private void btnAddSOP_Click(object sender, RoutedEventArgs e) {

      if (!helper.sopAdd(tbCompany.Text, tbIPAddress.Text)) {
        helper.sopDelete(tbIPAddress.Text);
        helper.sopAdd(tbCompany.Text, tbIPAddress.Text);

        if ((bool)chkPimary.IsChecked)
          helper.sopSetActive(tbCompany.Text, tbIPAddress.Text);
      }

      dgSopList.DataContext = helper.getSOPlist();

      tbCompany.Text = "";
      tbIPAddress.Text = "";
      chkPimary.IsChecked = false;
    }

    private void btnDelSOP_Click(object sender, RoutedEventArgs e) {
      helper.sopDelete(tbIPAddress.Text);

      dgSopList.DataContext = helper.getSOPlist();

      tbCompany.Text = "";
      tbIPAddress.Text = "";
      chkPimary.IsChecked = false;
    }

    private void dgSopList_SelectionChanged(object sender, SelectionChangedEventArgs e) {
      if (e.AddedItems.Count == 0)
        return;

      try {
        DataRowView selection = (e.AddedItems[0] as DataRowView);
        tbCompany.Text = selection.Row["company"].ToString();
        tbIPAddress.Text = selection.Row["ip"].ToString();
        chkPimary.IsChecked = Convert.ToBoolean(selection.Row["isprimary"]);


      }
      catch (Exception ex){
        string error = ex.Message; 
      }
    }

    private void btnSyncSopUsers_Click(object sender, RoutedEventArgs e) {
      mprProgress.IsActive = true;
      btnAddSOP.IsEnabled = false;
      btnDelSOP.IsEnabled = false;
      btnSyncSopUsers.IsEnabled = false;

      BackgroundWorker synch = new BackgroundWorker();
      synch.WorkerReportsProgress = false;
      synch.RunWorkerCompleted += Synch_RunWorkerCompleted;
      synch.DoWork += Synch_DoWork;
      synch.RunWorkerAsync();

    }

    private void Synch_DoWork(object sender, DoWorkEventArgs e) {
      DataTable SOPs = helper.getSOPlist();
      DataTable sopUsers = new DataTable();
      DataTable secuUsers = helper.getUserList();

      try {
        foreach (DataRow subSOP in SOPs.Select("isprimary=1")) {
          DataTable tmpUsers = helper.getUserDirectory(subSOP["ip"].ToString());

          foreach (DataRow sopUser in tmpUsers.Rows) {
            sopUser["sop"] = subSOP["company"].ToString();
          }
          tmpUsers.AcceptChanges();

          try {
            sopUsers.Merge(tmpUsers);
          }
          catch (Exception ex) {
            string error = ex.Message;
          }

          tmpUsers.Dispose();
        }
        //sopUsers.DefaultView.Sort = "lastname, firstname";
        e.Result += helper.syncPhoneUsers(ref sopUsers, ref secuUsers);
      }
      catch (Exception ex) {
        e.Result += ex.Message;
        //ModernDialog.ShowMessage(ex.Message, "error", MessageBoxButton.OK);
      }
      finally {
        SOPs.Dispose();
        sopUsers.Dispose();
        secuUsers.Dispose();
      }
      
    }

    private void Synch_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
      btnAddSOP.IsEnabled = true;
      btnDelSOP.IsEnabled = true;
      btnSyncSopUsers.IsEnabled = true;
      mprProgress.IsActive = false;

      if (e.Result.ToString() != "")
        ModernDialog.ShowMessage(e.Result.ToString(), "error", MessageBoxButton.OK);
      
    }
  }

  public class IntToBoolConverter : IValueConverter {

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
      try {
        if (value.ToString() == "1")
          return true;
        else
          return false;
      }
      catch (Exception ex) {
        string error = ex.Message;
        return false;
      }
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
      throw new NotImplementedException();
    }
  }
}
