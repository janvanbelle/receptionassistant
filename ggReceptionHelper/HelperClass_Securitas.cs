﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ggReceptionHelper {

  public partial class HelperClass_Reception : IDisposable {
    SqlConnection secuConnection;

    public bool openSecuConnection() {
      bool result = false;
      string AEOS_Database = getSetting("AEOS_CONNECT");

      secuConnection = new SqlConnection(AEOS_Database);

      try {
        secuConnection.Open();
        result = true;
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      return result;
    }

    public bool TestSecuConnection (string connectstring) {
      bool result = false;

      secuConnection = new SqlConnection(connectstring);

      secuConnection.Open();
      result = true;
      secuConnection.Close();

      return result;
    }

    public bool closeSecuConnection() {
      bool result = false;

      if (secuConnection.State != ConnectionState.Closed)
        secuConnection.Close();

      secuConnection.Dispose();
      secuConnection = null;

      return result;
    }

    public DataTable getSecuEntrances() {
      DataTable result = new DataTable("entrances");

      SqlDataAdapter entranceAdapter = new SqlDataAdapter(
        "select e.[objectid] as 'entranceid', e.[name], e.[description]," +
        "       coalesce(e.[autounlockscheduleid], -1) as 'unlockscheduleid'," +
        "     coalesce(ds.[name], '') as 'unlockschedule'" +
        "  from [entrance] e" +
        "       left outer join [datetimeschedule] ds on ds.[objectid] = e.[autounlockscheduleid]", secuConnection);

      try {
        entranceAdapter.Fill(result);
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
      finally {
        entranceAdapter.Dispose();
      }

      return result;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public DataTable getSecuUsers() {
      DataTable result = new DataTable("users");

      SqlDataAdapter userAdapter = new SqlDataAdapter(
        "SELECT ta.[carrierobjectid] as 'badgeid', t.[badgenumber] as 'badge', c.[imt] as 'firstname', c.[lastname], ffa.[value] as 'company'," +
        "       c.[personnelnr], c.[lasttimeseen]" +
        "  FROM [token] t," +
        "       [tokenassignment] ta " +
        "	      left outer join [carrier] c on c.[carrierobjectid] = ta.[carrierobjectid]" +
        "       left outer join [freefieldassignment] ffa on ffa.[carrierobjectid] = ta.[carrierobjectid]" +
        " where ta.[withdrawn] = 0" +
        "   and t.[blocked] = 'N'" +
        "   and t.[objectid] = ta.[identifierobjectid]" +
        "   and ((c.[leavedatetime] is null) or" +
        "        (c.[leavedatetime] > getdate()))", secuConnection);

      try {
        userAdapter.Fill(result);

        foreach (DataRow resultRow in result.Rows) {
          resultRow["firstname"] = resultRow["firstname"].ToString().Replace('é', 'e').Replace('è', 'e').Replace('à', 'a').Replace('ô', 'o').Replace('ç', 'c').TrimEnd(' ');
          resultRow["lastname"] = resultRow["lastname"].ToString().Replace('é', 'e').Replace('è', 'e').Replace('à', 'a').Replace('ô', 'o').Replace('ç', 'c').TrimEnd(' ');
        }

        result.AcceptChanges();
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
      finally {
        userAdapter.Dispose();
      }

      return result;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="carrierid"></param>
    /// <param name="fromTime"></param>
    /// <param name="toTime"></param>
    /// <returns></returns>
    public DataTable getSecuEvents(int carrierid, DateTime fromTime, DateTime toTime) {
      DataTable result = new DataTable("events");
      string queryString = "";

      //SqlDataAdapter eventAdapter = new SqlDataAdapter(
      //  "select ev.[timestamp], ev.[eventtype], ev.[carrierid], c.[carrierobjectid], t.[badgelabel]," +
      //  "       c.[personnelnr], c.[imt] as 'firstname', c.[lastname], ffa.[value] as 'company'," +
      //  "	      ev.[entranceid], e.[name], ev.[intvalue]" +
      //  "  from [entrance] e," +
      //  "       [eventlog] ev" +
      //  "	      left outer join [carrier] c on c.[carrierobjectid] = ev.[carrierid]" +
      //  "	      left outer join [token] t on t.[objectid] = ev.[identifierid]" +
      //  "	      left outer join [freefieldassignment] ffa on ffa.[carrierobjectid] = ev.[carrierid]" +
      //  " where e.[objectid] = ev.[entranceid]" +
      //  "   and ev.[carrierid] = " + carrierid.ToString() +
      //  "   and ev.[timestamp] between @fromTime and @toTime", secuConnection);

      if (carrierid == -1) {
        queryString =
          "select ev.[timestamp], ev.[eventtype], ev.[carrierid], c.[carrierobjectid], t.[badgelabel]," +
          "       c.[personnelnr], c.[imt] as 'firstname', c.[lastname], ffa.[value] as 'company'," +
          "	      ev.[entranceid], e.[name], ev.[intvalue]," +
          "       coalesce(br.[name] + ' (' + CONVERT(varchar, t.[dateblocked], 103) + ')', '') as 'extra'" +
          "  from [entrance] e," +
          "       [eventlog] ev" +
          "	      left outer join [carrier] c on c.[carrierobjectid] = ev.[carrierid]" +
          "	      left outer join [token] t on t.[objectid] = ev.[identifierid]" +
          "	      left outer join [freefieldassignment] ffa on ffa.[carrierobjectid] = ev.[carrierid]" +
          "       left outer join [blockreason] br on br.[objectid] = t.[reasonblocked]" +
          " where e.[objectid] = ev.[entranceid]" +
          "   and ev.[timestamp] between @fromTime and @toTime";
      } else {
        queryString =
          "select ev.[timestamp], ev.[eventtype], ev.[carrierid], c.[carrierobjectid], t.[badgelabel]," +
          "       c.[personnelnr], c.[imt] as 'firstname', c.[lastname], ffa.[value] as 'company'," +
          "	      ev.[entranceid], e.[name], ev.[intvalue]," +
          "       coalesce(br.[name] + ' (' + CONVERT(varchar, t.[dateblocked], 103) + ')', '') as 'extra'" +
          "  from [entrance] e," +
          "       [eventlog] ev" +
          "	      left outer join [carrier] c on c.[carrierobjectid] = ev.[carrierid]" +
          "	      left outer join [token] t on t.[objectid] = ev.[identifierid]" +
          "	      left outer join [freefieldassignment] ffa on ffa.[carrierobjectid] = ev.[carrierid]" +
          "       left outer join [blockreason] br on br.[objectid] = t.[reasonblocked]" +
          " where e.[objectid] = ev.[entranceid]" +
          "   and ev.[carrierid] = " + carrierid.ToString() +
          "   and ev.[timestamp] between @fromTime and @toTime";
      }

      SqlDataAdapter eventAdapter = new SqlDataAdapter(queryString, secuConnection);

      try {
        eventAdapter.SelectCommand.Parameters.AddWithValue("@fromTime", fromTime);
        eventAdapter.SelectCommand.Parameters.AddWithValue("@toTime", toTime);
        eventAdapter.Fill(result);
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
      finally {
        eventAdapter.Dispose();
      }

      return result;

    }

    public DataTable getSecuEvents(DateTime fromTime, DateTime toTime) {
      DataTable result = new DataTable("events");

      SqlDataAdapter eventAdapter = new SqlDataAdapter(
        "select ev.[timestamp], ev.[eventtype], ev.[carrierid], c.[carrierobjectid], t.[badgelabel]," +
        "       c.[personnelnr], c.[imt] as 'firstname', c.[lastname], ffa.[value] as 'company'," +
        "	      ev.[entranceid], e.[name], ev.[intvalue]," +
        "       c.[imt] + ' ' + c.[lastname] as 'fullname'" +
        "  from [entrance] e," +
        "       [eventlog] ev" +
        "	      left outer join [carrier] c on c.[carrierobjectid] = ev.[carrierid]" +
        "	      left outer join [token] t on t.[objectid] = ev.[identifierid]" +
        "	      left outer join [freefieldassignment] ffa on ffa.[carrierobjectid] = ev.[carrierid]" +
        " where e.[objectid] = ev.[entranceid]" +
        "   and ev.[carrierid] is not null" + 
        "   and ev.[timestamp] between @fromTime and @toTime" +
        " order by ffa.[value], c.[imt], c.[lastname], ev.[timestamp]", secuConnection);

      try {
        eventAdapter.SelectCommand.Parameters.AddWithValue("@fromTime", fromTime);
        eventAdapter.SelectCommand.Parameters.AddWithValue("@toTime", toTime);
        eventAdapter.Fill(result);
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
      finally {
        eventAdapter.Dispose();
      }

      return result;
    }

    public string isUserPresent(int carrierid) {
      string result = "";

      openSecuConnection();
      DataTable events = getSecuEvents(carrierid, DateTime.Today, DateTime.Now);
      closeSecuConnection();
      DataTable entrances = getEntranceList();

      // geen events: niet aanwezig op GG126
      if (events.Rows.Count == 0) {
        events.Dispose();
        entrances.Dispose();

        return result;
      }

      try {
        foreach (DataRow secuEvent in events.Rows) {
          // 1) ophalen toegangs-gegevens
          DataRow[] thisEntrance = entrances.Select("controllerid=" + secuEvent["entranceid"].ToString());

          if (Convert.ToInt32(thisEntrance[0]["toextern"]) == 0)
            continue;

          if (Convert.ToInt32(secuEvent["intvalue"]) == 1) {
            // persoon komt binnen
            result = thisEntrance[0]["zone"].ToString();
          } else {
            // persoon gaat buiten
            result = "";
          }
        }
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
      finally {
        events.Dispose();
        entrances.Dispose();
      }

      return result;
    }

    public DataSet getUserPresence(DateTime checkTime) {
      DataSet result = new DataSet();

      openSecuConnection();
      DataTable events = getSecuEvents(checkTime.Date, checkTime);
      closeSecuConnection();
      DataTable entrances = getEntranceList();

      DataTable locations = new DataTable("locations");
      locations.Columns.Add("location");
      locations.PrimaryKey = new DataColumn[] { locations.Columns[0] };

      DataTable presence = new DataTable("presence");
      presence.Columns.Add("location");
      presence.Columns.Add("company");
      presence.Columns.Add("name");

      try {
        int curCarrier = -1;
        bool isPresent = false;
        string userLocation = "", userCompany = "" , userName = "";
        foreach (DataRow secuEvent in events.Rows) {
          // 1) ophalen toegangs-gegevens
          DataRow[] thisEntrance = entrances.Select("controllerid=" + secuEvent["entranceid"].ToString());

          // 2) gaat het om een nieuw persoon?
          if (curCarrier != Convert.ToInt32(secuEvent["carrierid"])) {

            if (isPresent) {
              try {
                DataRow zoneRow = locations.NewRow();
                zoneRow["location"] = userLocation;
                locations.Rows.Add(zoneRow);
              }
              catch (Exception ex) {
                string error = ex.Message;
              }

              try {
                DataRow userPresence = presence.NewRow();
                userPresence["location"] = userLocation;
                userPresence["company"] = userCompany;
                userPresence["name"] = userName;
                presence.Rows.Add(userPresence);
              }
              catch (Exception ex) {
                string error = ex.Message;
              }
            }

            curCarrier = Convert.ToInt32(secuEvent["carrierid"]);
            isPresent = false;
            userLocation = "";
            userCompany = secuEvent["company"].ToString();
            userName = secuEvent["fullname"].ToString();

          }

          if (Convert.ToInt32(thisEntrance[0]["toextern"]) == 0)
            continue;

          if (Convert.ToInt32(secuEvent["intvalue"]) == 1) {
            // persoon komt binnen
            userLocation = thisEntrance[0]["zone"].ToString();
            isPresent = true;

          } else {
            // persoon gaat buiten
            isPresent = false;            
          }
        }

        if (isPresent) {
          try {
            DataRow zoneRow = locations.NewRow();
            zoneRow["location"] = userLocation;
            locations.Rows.Add(zoneRow);
          }
          catch (Exception ex) {
            string error = ex.Message;
          }

          try {
            DataRow userPresence = presence.NewRow();
            userPresence["location"] = userLocation;
            userPresence["company"] = userCompany;
            userPresence["name"] = userName;
            presence.Rows.Add(userPresence);
          }
          catch (Exception ex) {
            string error = ex.Message;
          }
        }
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
      finally {
        events.Dispose();
        entrances.Dispose();
      }

      result.Tables.Add(presence);
      result.Tables.Add(locations);

      return result;
    }

    public string syncSecuUsers() {
      string result = "";

      DataTable secuUsers = getSecuUsers();
      DataTable localUsers = getUserList();

      startTransaction();
      // gebruikers toevoegen of updaten
      foreach (DataRow secuUser in secuUsers.Rows) {
        DataRow[] localUser = new DataRow[0];
        try {
          string selectFilter = "badgeid=" + secuUser["badgeid"].ToString();
          localUser = localUsers.Select(selectFilter);
        }
        catch { }

        if (localUser.Length == 0) {
          addUser(Convert.ToInt32(secuUser["badgeid"]), secuUser["badge"].ToString(),
            secuUser["firstname"].ToString(), secuUser["lastname"].ToString(), secuUser["company"].ToString());
        } else {
          updUser(Convert.ToInt32(secuUser["badgeid"]), "firstname", secuUser["firstname"].ToString());
          updUser(Convert.ToInt32(secuUser["badgeid"]), "lastname", secuUser["lastname"].ToString());
          updUser(Convert.ToInt32(secuUser["badgeid"]), "company", secuUser["company"].ToString());
        }
      }
      commitTransaction();

      // bestaande gebruikers verwijderen indien badge geblokkeerd is / verlopen is
      foreach (DataRow localUser in localUsers.Rows) {
        DataRow[] secuUser = new DataRow[0];
        try {
          string selectFilter = "badgeid=" + localUser["badgeid"].ToString();
          secuUser = secuUsers.Select(selectFilter);
        }
        catch { }

        if (secuUser.Length == 0) {
          delUser(Convert.ToInt32(localUser["badgeid"]));
        }
      }

      secuUsers.Dispose();
      localUsers.Dispose();

      DataTable secuEntrances = getSecuEntrances();

      foreach (DataRow access in secuEntrances.Rows) {
        addEntrance(Convert.ToInt32(access["entranceid"]), access["name"].ToString(), Convert.ToInt32(access["unlockscheduleid"]), access["unlockschedule"].ToString());
      }

      secuEntrances.Dispose();

      return result;
    }

  }

}
