﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using ggReceptionHelper;
using FirstFloor.ModernUI.Windows.Controls;

namespace ggReceptionAssistant.General {
  /// <summary>
  /// Interaction logic for ucSearchUser.xaml
  /// </summary>
  public partial class ucSearchUser : UserControl {
    HelperClass_Reception helper;
    bool isAdmin;

    public ucSearchUser() {
      InitializeComponent();

      helper = ((MainWindow)(Application.Current.MainWindow)).helper;
      isAdmin = ((MainWindow)(Application.Current.MainWindow)).isAdminVersion;

    }

    private void btnSearchUser_Click(object sender, RoutedEventArgs e) {
      DataTable searchResult = helper.getUsers(tbSearchUser.Text);
      searchResult.Columns.Add("ispresent", typeof(string));

      foreach (DataRow user in searchResult.Rows) {
        int badgeid = Convert.ToInt32(user["badgeid"]);

        if (badgeid < 0) {
          user.Delete();
        } else {
          user["ispresent"] = helper.isUserPresent(badgeid);
        }
      }

      searchResult.AcceptChanges();

      dgUserSearch.DataContext = searchResult;
    }

    private void tbSearchUser_KeyDown(object sender, KeyEventArgs e) {
      try {
        if (e.Key == Key.Enter)
          btnSearchUser.Focus();
      }
      catch { }
    }

    private void ModernButton_Click(object sender, RoutedEventArgs e) {
      ModernButton sourceBtn = sender as ModernButton;

      HelperClass_Reception.phoneData phoneState = (HelperClass_Reception.phoneData)Application.Current.Resources["phoneState"];
      string sopIP = Application.Current.Resources["sop_ip"].ToString();
      string myPhone = Application.Current.Resources["phone_id"].ToString();
      string calledNumber = sourceBtn.Content.ToString();
      if (phoneState.state[0] != "Idle") {

        string displayMessage =
          "There is still a call ongoing\r\n" +
          "Would you like to park the current call?\r\n" +
          "Press 'Yes' to park, 'No' to abort";

        if (ModernDialog.ShowMessage(displayMessage, "question", MessageBoxButton.YesNo) == MessageBoxResult.No)
          return;

        helper.callPark(sopIP, phoneState.remoteChannel[0]);

        while (phoneState.state[0] != "Idle") {
          phoneState = helper.getPhoneState(sopIP, myPhone);
          Thread.Sleep(250);
        }

      } else {
        string displayMessage =
          "Would you like to call number '" + calledNumber + "' ?";

        if (ModernDialog.ShowMessage(displayMessage, "question", MessageBoxButton.YesNo) == MessageBoxResult.No)
          return;

      }

      calledNumber = "0" + calledNumber.Replace("+", "00");

      helper.callInitiate(sopIP, myPhone, calledNumber);
      ((MainWindow)Application.Current.MainWindow).ContentSource = new Uri("/General/ucMyCalls.xaml", UriKind.Relative);

    }
  }
}
