﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
//using System.Windows.Forms;
using System.Xml;

using Microsoft.Win32;

namespace ggReceptionHelper {

  public partial class HelperClass_Reception : IDisposable {
    SQLiteConnection liteConnection;
    SQLiteCommand liteCommand;

    #region construction

    public bool initSQLite(bool init) {
      bool result = false;
      try {
        liteConnection = new SQLiteConnection(SQLite_Database);
        liteConnection.Open();

        liteCommand = new SQLiteCommand(liteConnection);

        if (init) {
          CreateSqlData("ggReceptionHelper.Resources.database_sqlite.sql");

          if (getSetting("DOORUSER") == "no_value") {
            CreateSqlData("ggReceptionHelper.Resources.data_sqlite.sql");
          }
        }
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
      return result;
    } 

    private void CreateSqlData(string resname) {
      Assembly thisAssembly = Assembly.GetExecutingAssembly();
#if DEBUG
    //  string[] myResources = thisAssembly.GetManifestResourceNames();
#endif
      StreamReader dbDDL = new StreamReader(thisAssembly.GetManifestResourceStream(resname));


      bool inBeginEnd = false;
      liteCommand.CommandText = "";

      while (!dbDDL.EndOfStream) {
        string tmpLine = dbDDL.ReadLine();


        if (tmpLine.ToUpper().Contains("BEGIN"))
          inBeginEnd = true;

        if (tmpLine.ToUpper().StartsWith("END"))
          inBeginEnd = false;

        liteCommand.CommandText += tmpLine;

        if (tmpLine.Contains(";") &&
            !inBeginEnd) {
          try {
            liteCommand.ExecuteNonQuery();
          }
          catch (Exception ex) {
            string error = ex.Message;
          }
          liteCommand.CommandText = "";
        }
      }
    }
    #endregion

    #region GENERAL actions
    public void startTransaction() {
      liteCommand.Transaction = liteConnection.BeginTransaction();
    }

    public void commitTransaction() {
      liteCommand.Transaction.Commit();
    }
    #endregion

    #region SETTINGs actions
    public string getSetting(string parm) {
      string result = "no_value";

      liteCommand.CommandText =
        "select [value]" +
        "  from [Settings]" +
        " where [parm] = '" + parm.ToUpper() + "'";

      try {
        result = liteCommand.ExecuteScalar().ToString();
      }
      catch (Exception ex) {
        string error = ex.Message; 
      }

      return result;
    }

    public string addSetting(string parm, string value) {
      string result = "";

      liteCommand.CommandText =
        "insert into [Settings]" +
        "  values ('" + parm + "','" + value + "')";

      try {
        int addCnt = liteCommand.ExecuteNonQuery();
        if (addCnt == 0)
          result = "no new record for parm '" + parm + "' and value '" + value + "'";
      }
      catch (Exception ex) {
        result = ex.Message;
      }

      return result;
    }

    public string delSetting(string parm) {
      string result = "";

      liteCommand.CommandText =
        "delete from [Settings]" +
        " where [parm] = '" + parm + "'";

      try {
        int delCnt = liteCommand.ExecuteNonQuery();

        if (delCnt == 0)
          result = "no record deleted for parm '" + parm + "'";
      }
      catch (Exception ex) {
        result = ex.Message;
      }

      return result;
    }
    #endregion

    #region SOP actions
    public DataTable getSOPlist() {
      DataTable result = new DataTable("SOPs");

      SQLiteDataAdapter sopAdapter = new SQLiteDataAdapter(
        "select [company], [isprimary], [ip], [lastping]" +
        "  from [SopInfo]" , liteConnection);

      try {
        sopAdapter.Fill(result);
      }
      catch { }

      return result;
    }

    public string getSOPip(string company, bool isprimary) {
      string result = "0.0.0.0";

      liteCommand.CommandText =
        "select [ip]" +
        "  from [SopInfo]" +
        " where [company] = '" + company + "'" +
        "   and [isprimary] = "+ (isprimary ? "1" : "0");

      try {
        result = liteCommand.ExecuteScalar().ToString();
      }
      catch (Exception ex) {
        result = ex.Message;
      }

      return result;
    }

    public bool sopAdd(string company, string ip) {
      bool result = false;

      liteCommand.CommandText =
        "insert into [SopInfo] (company, isprimary, ip)" +
        "  values ('" + company + "', 0,'" + ip +"')"; 

      try {
        result = liteCommand.ExecuteNonQuery() == 1;
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      return result;
    }

    public bool sopSetActive(string company, string ip) {
      bool result = false;

      liteCommand.CommandText =
        "update [SopInfo]" +
        "   set [isprimary] = 0" +
        " where [company] = '" + company + "'"; // +
        //"   and [ip] = '" + ip + "'";

      try {
        liteCommand.ExecuteNonQuery();
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      liteCommand.CommandText =
       "update [SopInfo]" +
       "   set [isprimary] = 1" +
       " where [company] = '" + company + "'" +
       "   and [ip] = '" + ip + "'";

      try {
        result = liteCommand.ExecuteNonQuery() == 1;
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      return result;
    }

    public bool sopDelete(string ip /*, bool isprimary = false*/) {
      bool result = false;

      /*
      liteCommand.CommandText =
        "delete from [SopInfo]" +
        " where [company] = '" + company + "'" +
        "   and [isprimary] = " + (isprimary ? "1" : "0");
      */

      liteCommand.CommandText =
        "delete from [SopInfo]" +
        " where [ip] = '" + ip + "'";

      try {
        result = liteCommand.ExecuteNonQuery() == 1;
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      return result;
    }
    #endregion

    #region USER actions
    public DataTable getUserList() {
      DataTable result = new DataTable("users");

      SQLiteDataAdapter phoneAdapter = new SQLiteDataAdapter(
        "select [badgeid], [badge], [firstname], [lastname], [extension], [mobile], " +
        "       [company], [sop], [phoneid]" +
        "  from [Users]" +
        " order by [company], [lastname], [firstname]", liteConnection);

      try {
        phoneAdapter.Fill(result);
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
      finally {
        phoneAdapter.Dispose();
      }

      return result;
    }

    public DataTable getPhoneList(string sop) {
      DataTable result = new DataTable("phones");

      SQLiteDataAdapter phoneAdapter = new SQLiteDataAdapter(
        "select [firstname] || ' ' || [lastname] as 'Name', [phoneid]" +
        "  from [Users]" +
        " where [sop] = '" + sop + "'" +
        "   and [phoneid] <> ''" +
        " order by Name", liteConnection);

      try {
        phoneAdapter.Fill(result);
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
      finally {
        phoneAdapter.Dispose();
      }

      return result;
    }

    public DataTable getUsers(string filter) {
      DataTable result = new DataTable("users");

      SQLiteDataAdapter userAdapter = new SQLiteDataAdapter(
        "select [badgeid], [company], [sop], [extension], [firstname] || ' ' || [lastname] as 'Name', [mobile]" +
        "  from [Users]" +
        " where [firstname] like '%" + filter + "%'" +
        "    or [lastname] like '%" + filter + "%'", liteConnection);

      try {
        userAdapter.Fill(result);
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
      finally {
        userAdapter.Dispose();
      }

      return result;
    }

    public string getCaller(string callerid, string SOP) {
      string result = "";

      if (callerid.StartsWith("00"))
        callerid = callerid.Remove(0, 1);

      if (callerid.StartsWith("0032"))
        callerid = callerid.Replace("0032", "+32");

      // 1) opzoeken beller in lijst bestaande gebruikers
      liteCommand.CommandText =
        "select [firstname] || ' ' || [lastname]" +
        "  from [Users]" +
        " where [extension] = '" + callerid + "'" +
        "    or [mobile] = '" + callerid + "'";

      try {
        result = liteCommand.ExecuteScalar().ToString();
        return result;
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      // 1) opzoeken beller in lijst externe bellers
      liteCommand.CommandText =
        "select [name]" +
        "  from [Callers]" +
        " where [phoneno] = '" + callerid + "'";

      try {
        result = liteCommand.ExecuteScalar().ToString();
        return result;
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      liteCommand.CommandText =
        "select [firstname] || ' ' || [lastname]" +
        "  from [Users]" +
        " where [sop] = '" + SOP + "'" +
        "   and [extension] like '%" + callerid + "'";

      try {
        result = liteCommand.ExecuteScalar().ToString();
        return result;
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      return result;
    }

    public int addUser(string SOP, string firstName, string lastName, string extension) {
      int result = -1;

      liteCommand.CommandText =
        "select min([badgeid]) - 1" +
        "  from [Users]";

      try {
        result = Convert.ToInt32(liteCommand.ExecuteScalar());

        if (result > 0)
          result = -1;
      }
      catch {
        result = -1;
      }

      addUser(result, "none", firstName, lastName, "none");
      updUser(result, "extension", extension);
      updUser(result, "sop", SOP);

      return result;
    }

    public string addUser(int badgeid, string badgelabel, string firstName, string lastName, string company) {
      string result = "";

      liteCommand.CommandText =
        "insert into [Users]" +
        " (badgeid, badge, firstname, lastname, company)" +
        " values " +
        " (" + badgeid.ToString() + ", '" + badgelabel + "', '" + firstName + "', '" + lastName + "', '" + company + "')";

      try {
        liteCommand.ExecuteNonQuery();
      }
      catch (Exception ex) {
        result = ex.Message;
      }

      return result;
    }

    public bool updUser(string SOP, string extension, string parm, string value) {
      bool result = false;

      /*
      liteCommand.CommandText =
        "update [Users]" +
        "   set [" + parm + "] = '" + value + "'" +
        " where [badgeid] = '" + SOP + "'" +
        "   and [extension] = '" + extension + "'";

      try {
        liteCommand.ExecuteNonQuery();
        result = true;
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
      */

      return result;
    }

    public string updUser(int badgeid, string parm, string value) {
      string result = "";

      liteCommand.CommandText =
        "update [Users]" +
        "   set [" + parm + "] = '" + value + "'" +
        " where [badgeid] = " + badgeid.ToString();

      try {
        liteCommand.ExecuteNonQuery();
      }
      catch (Exception ex) {
        result = ex.Message;
      }

      return result;
    } 

    public bool delUser(string SOP, string extension) {
      bool result = false;

      /*
      liteCommand.CommandText =
        "delete from [Users]" +
        " where [sop] = '" + SOP + "'" +
        "   and [extension] = '" + extension + "'";

      try {
        liteCommand.ExecuteNonQuery();
        result = true;
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
      */
      return result;
    }

    public string delUser(int badgeid) {
      string result = "";

      liteCommand.CommandText =
        "delete from [Users]" +
        " where [badgeid] = " + badgeid.ToString();

      try {
        liteCommand.ExecuteNonQuery();
        
      }
      catch (Exception ex) {
        result = ex.Message;
      }

      return result;
    }

    public bool delAllUsers(string SOP = "") {
      bool result = false;

      liteCommand.CommandText =
        "delete from [Users]" +
        " where [sop] = '" + SOP + "'";

      try {
        liteCommand.ExecuteNonQuery();
        result = true;
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      return result;
    }

    #endregion

    #region DOORPHONE actions
    public DataTable getDoorphoneList() {
      DataTable result = new DataTable("doorphones");

      SQLiteDataAdapter doorAdapter = new SQLiteDataAdapter(
        "select [sop], [identifier], [description], [ip], [hascamera]" +
        "  from [Doorphones]", liteConnection);

      try {
        doorAdapter.Fill(result);
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      return result;
    }

    public DataTable getDoorphone(string callerid, string sop = "intrimmo") {
      DataTable result = new DataTable("doorphone");

      SQLiteDataAdapter doorAdapter = new SQLiteDataAdapter(
        "select [ip], [hascamera], [description]" +
        "  from [Doorphones]" +
        " where [sop] = '" + sop + "'" +
        "   and [identifier] = '" + callerid + "'", liteConnection);

      try {
        doorAdapter.Fill(result);
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      return result;

    }

    public bool addDoorPhone(string SOP, string ID, string description, string IP, string hascam) {
      bool result = false;

      liteCommand.CommandText =
        "insert into [Doorphones] (sop, identifier, description, ip, hascamera)" +
        "  values ('" + SOP + "', '" + ID + "', '" + description + "', '" + IP + "', " + hascam + ")";

      try {
        liteCommand.ExecuteNonQuery();
        result = true;
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      return result;
    }

    public bool updDoorPhone(string SOP, string ID, string description, string IP, string hascam) {
      bool result = false;

      liteCommand.CommandText =
        "update [Doorphones]" +
        "   set [description] = '" + description + "'," +
        "       [ip] = '" + IP + "'," +
        "       [hascamera] = '" + hascam + "'" +
        " where [sop] = '" + SOP + "'" +
        "   and [identifier] = '" + ID + "'";  

      try {
        liteCommand.ExecuteNonQuery();
        result = true;
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      return result;
    }

    public bool delDoorPhone(string SOP, string ID) {
      bool result = false;

      liteCommand.CommandText =
        "delete from [Doorphones]" +
        "  where [sop] =  '" + SOP + "'" +
        "    and [identifier] = '" + ID + "'";

      try {
        liteCommand.ExecuteNonQuery();
        result = true;
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      return result;
    }

    #endregion

    #region ACCESS actions
    public DataTable getEntranceList() {
      DataTable result = new DataTable("entrances");

      SQLiteDataAdapter entranceAdapter = new SQLiteDataAdapter(
        "select [controllerid], [toextern], [name], [unlockscheduleid], [unlockschedule], [zone]" +
        "  from [AccessZones]", liteConnection);

      try {
        entranceAdapter.Fill(result);
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
      finally {
        entranceAdapter.Dispose();
      }

      return result;
    }

    public bool addEntrance(int id, string name, int unlockscheduleid, string unlockschedule) {
      bool result = false;

      liteCommand.CommandText =
        "insert into [AccessZones]" +
        "  (controllerid, name, unlockScheduleid, unlockschedule)" +
        " values " +
        "  (" + id.ToString() + ", '" + name + "', " + unlockscheduleid.ToString() + ", '" + unlockschedule + "')";

      try {
        liteCommand.ExecuteNonQuery();
        result = true;
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      return result;
    }

    public bool updEntrance(int id, string parm, string value) {
      bool result = false;

      liteCommand.CommandText =
        "update [AccessZones]" +
        "   set [" + parm + "] = " + (parm == "zone" ? "'" + value + "'" : value) +
        " where [controllerid] = " + id.ToString();

      try {
        result = (liteCommand.ExecuteNonQuery() == 1);
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
      
      return result;
    }

    public bool delEntrance(int id) {
      bool result = false;

      liteCommand.CommandText =
        "delete from [AccessZones]" +
        " where [controllerid] = " + id.ToString();

      try {
        result = (liteCommand.ExecuteNonQuery() == 1);
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      return result;
    }
    #endregion

    #region LOGGING actions
    /// <summary>
    /// 
    /// </summary>
    /// <param name="logData"></param>
    /// <returns></returns>
    public bool addCallLogLine(ref phoneData logData) {
      bool result = false;

      liteCommand.CommandText =
        "insert into [CallLog] values" +
        "  (@timestamp, '" + logData.state + "'," +
        "   '" + logData.callerID + "', '" + logData.localChannel + "', " +
        "   '" + logData.remoteChannel + "', '" + logData.lastCall + "')";

      try {
        liteCommand.Parameters.Clear();
        liteCommand.Parameters.AddWithValue("@timestamp", DateTime.Now);
        liteCommand.ExecuteNonQuery();
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
      
      return result;
    }

    #endregion
  }
}
