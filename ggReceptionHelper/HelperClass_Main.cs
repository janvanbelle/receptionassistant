﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ggReceptionHelper {

  public partial class HelperClass_Reception : IDisposable {
    private bool disposed = false;
    public string SQLite_Database;
    public string AEOS_Database;

    #region construction
    public void Dispose() {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing) {
      if (!disposed) {
        if (disposing) {
          // Free other state (managed objects).
          liteCommand.Dispose();
          
          if (liteConnection.State != ConnectionState.Closed)
            liteConnection.Close();

          if (secuConnection.State != ConnectionState.Closed)
            secuConnection.Close();

          liteConnection.Dispose();
          secuConnection.Dispose();
        }
        // Free your own state (unmanaged objects).

        // Set large fields to null.
        disposed = true;
      }
    }
    #endregion
  }
}
