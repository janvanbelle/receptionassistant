﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Printing;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Xml;

namespace ggReceptionHelper {

  public partial class HelperClass_Reception : IDisposable {
    List<string[]> printParams;
    DataSet printData;

    #region document printing
    public void printUserPresence(DateTime presenceTime, bool emergency) {
      XmlDocument xmlInput = new XmlDocument();
      XmlNode xmlLayout = null;
      Assembly thisAssembly = Assembly.GetExecutingAssembly();

#if DEBUG
      string[] myResources = thisAssembly.GetManifestResourceNames();
#endif

      // 1) ophalen van de user presence
      printData = getUserPresence(presenceTime);

      // 2) aanmaak printparameters
      printParams = new List<string[]>();
      printParams.Add(new string[] { "FROMTIME", presenceTime.Date.ToString("dd/MM/yyyy HH:mm") });
      printParams.Add(new string[] { "TOTIME", presenceTime.ToString("dd/MM/yyyy HH:mm") });

      // 3) ophalen van de printlayout
      StreamReader printLayout = null;
      try {
        if (emergency)
          printLayout = new StreamReader(thisAssembly.GetManifestResourceStream("ggReceptionHelper.Resources.printlayout_emergency.xml"));
        else
          printLayout = new StreamReader(thisAssembly.GetManifestResourceStream("ggReceptionHelper.Resources.printlayout_presence.xml"));

        xmlInput.Load(printLayout);
        xmlLayout = xmlInput.ChildNodes[0];
      }
      catch (Exception ex) {
        string error = ex.Message;
        return;
      }

      if (emergency)
        printFlowDocument("emergency", ref xmlLayout);
      else
        printFlowDocument("presence", ref xmlLayout);

      xmlLayout = null;
      xmlInput = null;

      GC.Collect();
    }

    public void printUserEvents(int carrierid, DateTime presenceTime) {
      XmlDocument xmlInput = new XmlDocument();
      XmlNode xmlLayout = null;
      Assembly thisAssembly = Assembly.GetExecutingAssembly();

#if DEBUG
      string[] myResources = thisAssembly.GetManifestResourceNames();
#endif
      openSecuConnection();
      DataTable userEvents = getSecuEvents(carrierid, presenceTime.Date, presenceTime);
      closeSecuConnection();
      printData = new DataSet();
      printData.Tables.Add(userEvents);

      // 3) aanmaak printparameters
      printParams = new List<string[]>();
      printParams.Add(new string[] { "FROMTIME", presenceTime.Date.ToString("dd/MM/yyyy HH:mm") });
      printParams.Add(new string[] { "TOTIME", presenceTime.ToString("dd/MM/yyyy HH:mm") });

      StreamReader printLayout = null;
      try {
        printLayout = new StreamReader(thisAssembly.GetManifestResourceStream("ggReceptionHelper.Resources.printlayout_events.xml"));

        xmlInput.Load(printLayout);
        xmlLayout = xmlInput.ChildNodes[0];
      }
      catch (Exception ex) {
        string error = ex.Message;
        return;
      }

      printFlowDocument("events", ref xmlLayout);

      xmlLayout = null;
      xmlInput = null;

      GC.Collect();
    }
    #endregion

    #region region processing
    private void printFlowDocument(string reference, ref XmlNode printLayout) {
      FlowDocument headerData = null, bodyData = null, footerData = null;

      foreach (XmlNode layoutNode in printLayout.ChildNodes) {

        switch (layoutNode.Name.ToLower()) {
        case "header":
          headerData = getFlowDocument(layoutNode);
          break;
        case "body":
          bodyData = getFlowDocument(layoutNode);
          break;
        case "footer":
          footerData = getFlowDocument(layoutNode);
          break;
        }
      }

      SimplePaginator sdp = null;
      PrintDialog pd = null;

      try {
        sdp = new SimplePaginator();

        if (headerData != null) sdp.SetHeader(headerData);
        if (bodyData != null) sdp.SetContent(bodyData);
        if (footerData != null) sdp.SetFooter(footerData);

        sdp.headerHeight = 100;
        sdp.footerHeight = 100;
#if DEBUG
        sdp.repeatTableHeaders = true;
#endif

        pd = new PrintDialog();
        pd.PrintDocument(sdp, reference);
      }
      catch (Exception ex) {
        string error = ex.Message;
      }
      finally {
        sdp = null;
        pd = null;
      }

      printParams.Clear();
      printParams = null;

    }

    private FlowDocument getFlowDocument(XmlNode xNode) {
      XmlDocument result = new XmlDocument();
      XmlElement rootElement = result.CreateElement("FlowDocument");
      DataTable flowData = null;

      foreach (XmlNode docNode in xNode.ChildNodes) {
        switch (docNode.Name.ToLower()) {
        case "query":
          flowData = fetchFlowData(docNode, null);
          break;

        default:
          if (flowData != null) {
            foreach (DataRow rowData in flowData.Rows) {
              XmlNode calcNode = processFlowData(docNode, rowData);
              XmlNode newNode = result.ImportNode(calcNode, true);
              rootElement.AppendChild(newNode);
            }
          } else {
            XmlNode calcNode = processFlowData(docNode, null);
            XmlNode newNode = result.ImportNode(calcNode, true);
            rootElement.AppendChild(newNode);
          }
          break;
        }
      }

      if (rootElement.OuterXml.Length > 17) {
        //xmlns = "http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        rootElement.SetAttribute("xmlns", "http://schemas.microsoft.com/winfx/2006/xaml/presentation");
        result.AppendChild(rootElement);

        //xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        rootElement.SetAttribute("xmlns:x", "http://schemas.microsoft.com/winfx/2006/xaml/");
        result.AppendChild(rootElement);

        using (var xmlReader = XmlReader.Create(new StringReader(result.OuterXml))) {
          try {
            return (FlowDocument)System.Windows.Markup.XamlReader.Load(xmlReader);
          }
          catch (XamlParseException ex) {
            string error = ex.Message;
          }
          catch (Exception ex) {
            string error = ex.Message;
          }
        }
      }

      return null;
    }
#endregion

#region element processing
    private DataTable fetchFlowData(XmlNode baseDoc, DataRow input) {
      DataTable result = null;
      bool useSQLite = false;
      string tableName = "";
      string[] thisParm = new string[2] { "", "" };

      foreach (XmlAttribute attribute in baseDoc.Attributes) {
        try {
          switch (attribute.Name) {
          case "Kind":
            if (attribute.Value == "SQLite")
              useSQLite = true;
            else
              useSQLite = false;
            break;

          case "Source":
            tableName = attribute.Value;
            break;

          case "Parm":
            thisParm[0] = attribute.Value;
            break;

          case "Value":
            thisParm[1] = attribute.Value;

            if ((input != null) &&
                (thisParm[1].Contains("Databind="))) {
              thisParm[1] = thisParm[1].Replace("Databind=", "");
              thisParm[1] = input[thisParm[1]].ToString();
            }
            printParams.Add(thisParm);
            break;
          }
        }
        catch (Exception ex) {
          string error = ex.Message;
        }
      }

      if (useSQLite)
        result = fetchFlowSQLite(baseDoc.FirstChild.Value);
      else
        result = fetchFlowLocal(tableName, ref thisParm);

      if ((input != null) &&
          (baseDoc.Attributes.Count > 0)) {
        printParams.Remove(thisParm);
      }

      return result;
    }

    private DataTable fetchFlowSQLite(string query) {
      DataTable result = new DataTable();

      SQLiteDataAdapter queryAdapter = new SQLiteDataAdapter(query, SQLite_Database);
      foreach (string[] parm in printParams) {
        queryAdapter.SelectCommand.Parameters.AddWithValue("@" + parm[0], parm[1]);
      }
      try {
        queryAdapter.Fill(result);
      }
      catch (Exception ex) {
        string error = ex.Message;
        result = new DataTable();
      }
      finally {
        queryAdapter.Dispose();
      }

      return result;
    }

    private DataTable fetchFlowLocal(string table, ref string[] parms) {
      DataTable result = null;

      try {
        if (parms[0] != "") {
          DataView tmpView = new DataView(printData.Tables[table]);
          tmpView.RowFilter = parms[0] + " = '" + parms[1] + "'";

          result = tmpView.ToTable();
        } else {
          result = printData.Tables[table];
        }
      }
      catch (Exception ex) {
        string error = ex.Message;
        result = new DataTable();
      }

      return result;
    }

    private XmlNode processFlowData(XmlNode baseDoc, DataRow input) {
      XmlNode result = null;
      DataTable subFlowData = null;

      // 1) de node clonen
      result = baseDoc.CloneNode(false);
      try {
        foreach (XmlAttribute attribute in result.Attributes) {

          if (attribute.Value.StartsWith("DataBind=")) {
            string colName = attribute.Value.Replace("DataBind=", "");

            attribute.Value = input[colName].ToString();
          }
        }
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      try {
        if (result.InnerText.StartsWith("DataBind=")) {
          string colName = result.InnerText.Replace("DataBind=", "");

          result.InnerText = input[colName].ToString();
        }
      }
      catch (Exception ex) {
        string error = ex.Message;
      }

      foreach (XmlNode subNode in baseDoc.ChildNodes) {
        switch (subNode.Name.ToLower()) {
        case "query":
          subFlowData = fetchFlowData(subNode, input);
          break;

        default:
          if (subFlowData != null) {
            foreach (DataRow rowData in subFlowData.Rows) {
              result.AppendChild(processFlowData(subNode, rowData));
            }
          } else {
            result.AppendChild(processFlowData(subNode, input));
          }
          break;
        }
      }

      return result;
    }
#endregion
  }
}
