﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using ggReceptionHelper;

namespace ggReceptionAssistant.Settings {
  /// <summary>
  /// Interaction logic for ucSettingsDoorphone.xaml
  /// </summary>
  public partial class ucSettingsDoorphone : UserControl {
    //HLP_SQLite sqlHelper;
    //HLP_Escaux telHelper;
    HelperClass_Reception helper;
    bool isAdmin;

    public ucSettingsDoorphone() {
      InitializeComponent();

      //sqlHelper = ((MainWindow)(Application.Current.MainWindow)).sqlHelper;
      //telHelper = ((MainWindow)(Application.Current.MainWindow)).telHelper;
      helper = ((MainWindow)(Application.Current.MainWindow)).helper;
      isAdmin = ((MainWindow)(Application.Current.MainWindow)).isAdminVersion;

      tbCamUser.Text = helper.getSetting("DOORUSER");
      pwbCamPass.Password = helper.getSetting("DOORPASS");

      DataTable sopList = helper.getSOPlist();
      sopList.DefaultView.RowFilter = "isprimary = 1";
      cbbSopList.DataContext = sopList.DefaultView.ToTable();

      dgDoorphones.DataContext = helper.getDoorphoneList();

      if (isAdmin) {
        btnAddDoorphone.IsEnabled = true;
        btnDelDoorphone.IsEnabled = true;
        btnSaveCamUser.IsEnabled = true;
      } else {
        btnAddDoorphone.IsEnabled = false;
        btnDelDoorphone.IsEnabled = false;
        btnSaveCamUser.IsEnabled = false;
      }
    }

    private void btnSaveCamUser_Click(object sender, RoutedEventArgs e) {
      helper.addSetting("DOORUSER", tbCamUser.Text);
      helper.addSetting("DOORPASS", pwbCamPass.Password);
    }

    #region add / update / delete doorphones
    private void btnAddDoorphone_Click(object sender, RoutedEventArgs e) {
      if (btnAddDoorphone.Content.ToString() == "Add") {
        helper.addDoorPhone(cbbSopList.Text, tbCamID.Text, tbCamLocation.Text, tbCamIP.Text, ((bool)chkCameraPresent.IsChecked ? "1" : "0"));
      } else {
        helper.updDoorPhone(cbbSopList.Text, tbCamID.Text, tbCamLocation.Text, tbCamIP.Text, ((bool)chkCameraPresent.IsChecked ? "1" : "0"));
      }

      cbbSopList.SelectedIndex = -1;
      tbCamID.Text = "";
      tbCamIP.Text = "";
      tbCamLocation.Text = "";
      chkCameraPresent.IsChecked = false;

      cbbSopList.Tag = "";
      tbCamID.Tag = "";
      dgDoorphones.DataContext = helper.getDoorphoneList();
    }

    private void btnDelDoorphone_Click(object sender, RoutedEventArgs e) {
      helper.delDoorPhone(cbbSopList.Text, tbCamID.Text);

      cbbSopList.SelectedIndex = -1;
      tbCamID.Text = "";
      tbCamIP.Text = "";
      tbCamLocation.Text = "";
      chkCameraPresent.IsChecked = false;

      cbbSopList.Tag = "";
      tbCamID.Tag = "";
      dgDoorphones.DataContext = helper.getDoorphoneList();
    }
    #endregion

    private void dgDoorphones_SelectionChanged(object sender, SelectionChangedEventArgs e) {
      if (e.AddedItems.Count == 0)
        return;

      try {
        DataRowView selection = (e.AddedItems[0] as DataRowView);
        cbbSopList.Text = selection.Row["SOP"].ToString();
        tbCamID.Text = selection.Row["identifier"].ToString();
        tbCamIP.Text = selection.Row["ip"].ToString();
        tbCamLocation.Text = selection.Row["description"].ToString();
        chkCameraPresent.IsChecked = Convert.ToBoolean(selection.Row["hascamera"]);

        cbbSopList.Tag = selection.Row["SOP"].ToString();
        tbCamID.Tag = selection.Row["identifier"].ToString();

        btnAddDoorphone.Content = "Change";
        //tbCamIP.Tag = selection.Row["ip"].ToString();
        //tbCamLocation.Tag = selection.Row["description"].ToString();
        //chkCameraPresent.Tag = Convert.ToBoolean(selection.Row["hascamera"]);


      }
      catch (Exception ex) {
        string error = ex.Message;
      }
    }

    #region changed events
    private void tbCamID_TextChanged(object sender, TextChangedEventArgs e) {
      try {
        if (tbCamID.Text != tbCamID.Tag.ToString())
          btnAddDoorphone.Content = "Add";
        else
          btnAddDoorphone.Content = "Change";
      }
      catch { }
    }

    private void cbbSopList_SelectionChanged(object sender, SelectionChangedEventArgs e) {
      try {
        if (cbbSopList.Text != cbbSopList.Tag.ToString())
          btnAddDoorphone.Content = "Add";
        else
          btnAddDoorphone.Content = "Change";
      }
      catch { }
    }
    #endregion
  }
}
