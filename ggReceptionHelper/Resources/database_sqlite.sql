create table if not exists SopInfo (
  company      varchar,
  isprimary    integer,
  isenabled    integer default 1,
  ip           varchar,
  lastping     integer 
);
--create unique index if not exists SopIdx on SopInfo(company, isprimary);
create unique index if not exists SopIPs on SopInfo(ip);

create table if not exists Settings (
  parm         varchar,
  value        varchar
);
create unique index if not exists SettingIdx on Settings(parm);

 create table if not exists Users (
   badgeid     integer primary key,
   badge       varchar default '',
   company     varchar,
   department  varchar,
   firstname   varchar,
   lastname    varchar,
   sop         varchar default '',
   extension   varchar default '',
   mobile      varchar default '',
   email       varchar default '',
   phoneid     varchar default ''
 );
create index if not exists UserFNIdx on Users(firstname);
create index if not exists UserLNIdx on Users(lastname);
create index if not exists UserBadgeIdx on Users(badge);
create index if not exists UserBadgeIdx on Users(badge);

create table if not exists Doorphones (
  sop          varchar,
  identifier   varchar,
  description  varchar,
  ip           varchar,
  hascamera    integer
);

create table if not exists Callers (
  phoneno      varchar,
  name         varchar default '',
  company      varchar default '',
  badgeid      integer default -1
);

create table if not exists CallLog (
  logtime        datetime,
  callstate      varchar,
  callerid       varchar,
  localchannel   varchar,
  remotechannel  varchar,
  lastcall       varchar
);

create table if not exists AccessZones (
  controllerid      integer primary key,
  toextern          integer default -1,
  name              varchar,
  unlockscheduleid  integer,
  unlockschedule    varchar,
  zone              varchar default 'no_zone'
);

